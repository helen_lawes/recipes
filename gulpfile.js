var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	ignore = require('gulp-ignore'),
	config = require('./gulp.config').config,
	ts = require('gulp-typescript'),
	nodemon = require('nodemon'),
	plumber = require('gulp-plumber'),
	browserSync = require('browser-sync').create(),
	browserify = require('browserify'),
	watchify = require('watchify'),
	source = require('vinyl-source-stream'),
	path = require('path'),
	fs = require('fs');
var srcDir = __dirname +'/public-src/';
var destDir = __dirname+'/app/public/';

var tsProject = ts.createProject('tsconfig.json');

var opts = watchify.args;
opts.entries = [destDir+config.entryFile];
var b = watchify(browserify(opts));


gulp.task('sass', function(){
	return sass(srcDir+'css/*.scss', {style:'expanded'})
	.pipe(gulp.dest(destDir+'css'))
	.pipe(browserSync.stream({match: '**/*.css'}));
});
gulp.task('scripts', function(){

	var tsResult = tsProject.src()
		.pipe(ts(tsProject));

	return tsResult.js.pipe(gulp.dest(destDir+'js'));
});
gulp.task('bundle',bundle);
b.on('update',bundle);
function bundle(){
	return b.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest(destDir+'js'));
}
gulp.task('scripts-watch',gulp.series('scripts',browserSync.reload));
function excludeCopy(file){
	var ext = path.extname(file.path);
	return (ext == ".scss" || ext == ".ts" || file.path.match('sw.js'));
}
gulp.task('copy', function(){
	return gulp.src(srcDir+'**')
		.pipe(ignore.exclude(excludeCopy))
		.pipe(gulp.dest(destDir));
});
gulp.task('service-worker',function(callback){
	var files = config.cacheDefaults || [];
	var folders = [destDir+'/resources',destDir+'/tpl'];
	folders.forEach(function(folder){
		var folderFiles = fs.readdirSync(folder);
		folderFiles.forEach(function(file,i){
			var temp = path.join(path.relative(destDir,folder),file).split(path.sep);
			folderFiles[i] = '/'+temp.join('/');
		});
		files = files.concat(folderFiles);
	});
	var timestamp = new Date().getTime();
	var contents = fs.readFileSync(path.join(srcDir,'sw.js'));
	contents = "//"+timestamp+"\nvar cacheFiles = "+ JSON.stringify(files, null, '\t') + ";\n" + contents;
	fs.writeFileSync(path.join(destDir,'sw.js'),contents);
	callback();
});
gulp.task('build', gulp.parallel('sass','scripts','copy','service-worker'));
gulp.task('default',gulp.series('sass','scripts','copy','service-worker'));
gulp.task('watch', gulp.series('build',function(){
	browserSync.init({
		proxy: 'localhost:8080'
	});
	nodemon({
		script:'app/server.js',
		watch:['app'],
		ignore:['app/public']
	})
	.on('start',browserSync.reload);
	gulp.watch(srcDir+'css/*.scss',gulp.series('sass'));
	gulp.watch(srcDir+'js/*.ts',gulp.series('scripts-watch','service-worker'));
	gulp.watch(srcDir+'sw.js',gulp.series('service-worker'));
	gulp.watch([srcDir+'*.html',srcDir+'tpl/*.html'],gulp.series('copy'));
	gulp.watch(destDir+"*.html").on("change", browserSync.reload);
}));
