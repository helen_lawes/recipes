var config = {
    cacheDefaults:[
	    '/',
	    '/css/styles.css',
	    '/js/bundle.js',
        '/core-js/client/shim.min.js',
	    '/zone.js/dist/zone.js',
	    '/reflect-metadata/Reflect.js',
	    '/socket.io/socket.io.js'
    ],
	entryFile:'js/main.js'
};
if(exports){
    exports.config = config;
}