import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }   from './app.component';

import { MODAL_DIRECTIVES } from './modal.component';
import { FORMATTING_PIPES } from './formatting.pipes';
import { MENU_DIRECTIVES } from './menu.component';
import { FIELD_DIRECTIVES } from './field.component';
import { NOTIFICATION_DIRECTIVES } from './notification.component';

import { routing, appRoutingProviders, routeComponents } from './app.routes';


@NgModule({
	declarations: [AppComponent,MODAL_DIRECTIVES,MENU_DIRECTIVES,FORMATTING_PIPES,FIELD_DIRECTIVES,NOTIFICATION_DIRECTIVES,routeComponents],
	imports: [BrowserModule,routing,FormsModule,HttpModule,JsonpModule],
	bootstrap: [AppComponent],
	providers: [appRoutingProviders]
})
export class AppModule {}
