import { Injectable, EventEmitter } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Database } from './database';
import { ObjectId } from './utilities';
import { SocketService } from './socket.service';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/retry';

/**
 * Recipe service
 *
 * Interacts with the backend API
 */
@Injectable()
export class RecipesService{

	// event emitter for service worker
	swEvents: EventEmitter<any> = new EventEmitter();

	// base url for recipe api
	private baseUrl: string = '/api/recipes';
	private _liveData: any = [];
	private data: any = [];
	private status: string = 'init';
	private _dbPromise;
	private _dbName = 'recipes';
	private _dbStoreName = 'recipes';
	private _dbUp = true;
	private _dbEmpty = true;
	private _headers;
	private _recipeEmitter: EventEmitter<any> = new EventEmitter();
	private _swWorker;

	constructor(private _http: Http, private _db: Database, private _socket: SocketService){
		// register the service worker
		this._registerServiceWorker();
		// open the connection to the IndexedDB
		this._dbInit().then(()=>{
			// run live recipe sync when able to connect to servers socket.io
			this._socket.getConnection().subscribe(connection=>{
				if(connection) this._getLiveRecipesSync();
			})
		});
		// HTTP headers for requests
		this._headers = {
			json: new Headers({
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + window.localStorage.getItem('token')
			}),
			form: new Headers({
				'Authorization': 'Bearer ' + window.localStorage.getItem('token')
			})
		}

	}

	getRecipes(){

		if(this._dbUp){
			// get recipes from local database
			this._dbGetRecipes()
				.then(()=>{
					this._recipeEmitter.emit(this.data);
				});
		} else {
			// if local IndexedDB is offline then grab live data
			this._getLiveRecipesSync(false);
		}

		// Display a message to the user if local & live DBs are offline
		if(!this._dbUp && this.status == 'offline'){
			this._recipeEmitter.error('No database found');
		}

		return this._recipeEmitter;

	}

	getRecipe(id: string){
		return new Observable(observer =>{
			if(this._dbUp){
				this._dbGetRecipe(id).then(recipe=>observer.next(recipe)).catch( e => observer.error(e) );
			} else if(this.data.length){
				var match = this._filterById(this.data,id);
				if(match){
					observer.next(match);
					observer.complete();
				} else {
					observer.error('None found');
				}
			} else {
				this.getRecipes().subscribe(() =>{
					var match = this._filterById(this.data,id);
					if(match){
						observer.next(match);
						observer.complete();
					} else {
						observer.error('None found');
					}
				},
				e =>{
					observer.error(e);
				});
			}

			return () => {}
		});
	}

	private _filterById(data, id){
		var match = data.filter((h:{_id}) => h._id === id);
		return match[0] || false;
	}

	private _excludeByIds(data, ids){
		var matches = data.filter((h:{_id}) => !ids.includes(h._id));
		return matches.length ? matches : false;
	}

	getBlank(){
		// create a blank recipe object
		return Promise.resolve({
			"_id":new ObjectId().toString(),
			"photo" : "",
			"name" : "",
			"brief_description" : "",
			"tags" : [],
			"serves" : null,
			"time" : null,
			"ingredients" : [],
			"methods":[],
			"personal_notes" : "",
			"recipe_refs" : "",
			"category" : "",
			"timestamp": new Date().getTime()
		})
	}

	toFormObject(recipe){
		// convert object data into FormData to POST file information
		let file = recipe.file;
		delete recipe.file;
		var formData = new FormData();
		formData.append("recipe", JSON.stringify(recipe));
		formData.append("file", file);
		return formData;
	}

	updateRecipe(recipe){
		return new Promise(resolve =>{
			recipe.timestamp = new Date().getTime();
			// update the recipe data in the recipe service as the data is not edited live
			this.data.forEach((v,k) => {
				if(v._id == recipe._id){
					this.data[k] = recipe;
				}
			});
			// store recipe changes locally in DB
			this._dbStoreRecipe(recipe).then(resolve);
			// send recipe changes to live DB, but don't wait for response to speed up app response
			this._httpUpdateRecipe(recipe);

		});
	}

	private _httpUpdateRecipe(recipe){
		let options = new RequestOptions({headers: this._headers.form });
		let data = this.toFormObject(recipe);
		return this._http.put(`${this.baseUrl}/${recipe._id}`,data,options)
			.toPromise()
			.then(response => response.json());
	}

	createRecipe(recipe){
		return new Promise(resolve =>{
			// store the new recipe in the local DB
			this._dbStoreRecipe(recipe).then(resolve);
			// merge the new recipe in with the other recipes in the service
			this.data.push(recipe);
			// send newly created recipe to live DB
			this._httpCreateRecipe(recipe);
		});
	}

	private _httpCreateRecipe(recipe){
		let options = new RequestOptions({headers: this._headers.form });
		let data = this.toFormObject(recipe);
		return this._http.post(`${this.baseUrl}`,data,options)
			.toPromise()
			.then(response => response.json());
	}


	deleteRecipe(recipe){
		return new Promise(resolve =>{

			var index = this.data.indexOf(recipe);
			if(index > -1){
				this.data.splice(index,1);
			}
			this._dbDeleteRecipe(recipe._id).then(resolve);

			let options = new RequestOptions({headers: this._headers.json ,body:''});
			this._http.delete(`${this.baseUrl}/${recipe._id}`,options)
				.toPromise()
				.then(response => response.json())
				.catch(this.handleError);
		});
	}

	private _httpDeleteRecipes(ids){
		let options = new RequestOptions({headers: this._headers.json ,body:ids});
		this._http.delete(`${this.baseUrl}`,options)
			.toPromise()
			.then(response => response.json())
			.catch(this.handleError);
	}

	private _registerServiceWorker(){
		if (!navigator.serviceWorker) return;

		navigator.serviceWorker.register('/sw.js').then((reg)=>{
			if (!navigator.serviceWorker.controller) {
				return;
			}

			if(reg.waiting){
				this._swUpdateReady(reg.waiting);
				return;
			}

			if(reg.installing){
				this._swTrackInstall(reg.installing);
				return;
			}

			if(reg.active){
				this._swActive(reg.active);
			}

			reg.addEventListener('updatefound',()=>{
				this._swTrackInstall(reg.installing);
			});

		});

		var refreshing;
		navigator.serviceWorker.addEventListener('controllerchange',function(){
			if(refreshing) return;
			window.location.reload();
			refreshing = true;
		});
	}

	private _swTrackInstall(worker){
		worker.addEventListener('statechange',()=>{
			if(worker.state == 'installed'){
				this._swUpdateReady(worker);
			}
		});
	}

	private _swUpdateReady(worker){
		// emit service worker ready event
		this.swEvents.emit('ready');
		this._swWorker = worker;
	}

	private _swActive(worker){
		// emit service worker active event
		this.swEvents.emit('active');
		this._swWorker = worker;
	}

	swUpdateConfirmed(){
		// send message to service worker to update to latest version
		this._swWorker.postMessage({action:'skipWaiting'});
	}

	swClearPhotos(imagesUsed){
		if(this._swWorker){
			this._swWorker.postMessage({action:'clearPhotos',imagesUsed});
		} else {
			this.swEvents.subscribe(message=>{
				if(message=='ready'||message=='active') this._swWorker.postMessage({action:'clearPhotos',imagesUsed});
			})
		}
	}

	private _dbInit(){
		return this._dbPromise = this._db.open(this._dbName,2,(upgradeDb)=>{
			upgradeDb.createObjectStore(this._dbStoreName,{keyPath:'_id'});
		}).catch(()=>{
			this._dbUp = false;
		});
	}

	private _dbGetRecipes(){
		return new Promise((resolve,reject)=>{
			this._dbPromise.then((db)=>{
				var store = db.transaction(this._dbStoreName,'readwrite').objectStore(this._dbStoreName);
				store.getAll().then((recipes)=>{
					if(recipes.length){
						this._dbEmpty = false;
						this.data = recipes;
						resolve();
					}
				}).catch( e => reject(e) );
			});
		});
	}

	private _dbGetRecipe(id){
		return new Promise((resolve,reject)=>{
			this._dbPromise.then((db)=>{
				var store = db.transaction(this._dbStoreName,'readwrite').objectStore(this._dbStoreName);
				store.get(id).then((recipe)=>{
					resolve(recipe);
				}).catch( e => reject(e) );
			});
		});
	}



	private _dbStoreRecipes(recipes){
		return new Promise((resolve,reject)=>{
			this._dbPromise.then((db) =>{
				var store = db.transaction(this._dbStoreName,'readwrite').objectStore(this._dbStoreName);
				recipes.forEach((recipe) =>{
					store.put(recipe).catch( e => reject(e) );
				});
				resolve();
			});
		});
	}

	private _dbStoreRecipe(recipe){
		return new Promise((resolve,reject)=> {
			this._dbPromise.then((db) => {
				var store = db.transaction(this._dbStoreName, 'readwrite').objectStore(this._dbStoreName);
				store.put(recipe).then(()=> resolve(recipe)).catch(reject);
			});
		});
	}

	private _getLiveRecipesSync(sync = true){
		let options = new RequestOptions({headers: this._headers.json ,body:''});

		this._http.get(`${this.baseUrl}`,options)
			.retry(5)
			.subscribe((response: {json}) => {
				this._liveData = response.json();
				this.status = 'success';
				if(sync || !this._dbUp){
					this._dbSyncRecipes().then(()=>{
						this._recipeEmitter.emit(this.data);
					});
				} else {
					this.data = this._liveData;
				}
			},(e)=>{
				this.status = 'offline';
			});
	}

	private _dbSyncRecipes(){
		return new Promise((resolve,reject)=>{
			let syncTime = new Date().getTime();
			let lastSync = localStorage.getItem('dbSync')/1 || syncTime;
			let ids = [];
			let imagesUsed = [];
			this._dbPromise.then((db)=>{
				var store = db.transaction(this._dbStoreName,'readwrite').objectStore(this._dbStoreName);
				store.loopCursor((cursor)=>{
					let current = cursor.value;
					// check if current db record is in live data
					let match = this._filterById(this._liveData,current._id);
					let using = true;
					if(match){
						if(current.timestamp > match.timestamp){
							// if local db record more up-to-date then send update to server
							this._httpUpdateRecipe(current);
						} else {
							// if live db record more up-to-date then store in local
							this._dbStoreRecipe(match);
						}
					} else {
						if(current.timestamp > lastSync){
							// if local db record newer than last sync then send to server
							this._httpCreateRecipe(current).catch(()=>{
								current.timestamp = syncTime+5;
								this._dbStoreRecipe(current)
							})
						} else {
							// if local db record older than sync then delete from local
							using = false;
							cursor.delete();
						}
					}
					if(using) {
						ids.push(current._id);
						imagesUsed.push(current.photo);
					}
				}).then(()=>{
					let unsaved = this._excludeByIds(this._liveData,ids);
					let toSave = [];
					let toDelete = [];
					if(unsaved){
						// only save down recipes added after last sync as they have been added
						// older recipes would have been deleted
						unsaved.forEach(function(recipe){
							if(recipe.timestamp > lastSync || !ids.length){
								toSave.push(recipe);
							} else {
								toDelete.push(recipe._id);
							}
						});
						if(toSave.length) this._dbStoreRecipes(toSave);
						if(toDelete.length) this._httpDeleteRecipes(toDelete);
					}

					localStorage.setItem('dbSync',''+new Date().getTime());
					this.swClearPhotos(imagesUsed);
					this._dbGetRecipes().then(()=> resolve(this.data));
				}).catch(reject);

			}).catch(()=> {
				this.data = this._liveData;
				resolve();
			});
		});

	}

	private _dbDeleteRecipe(id){
		return new Promise((resolve,reject)=>{
			this._dbPromise.then((db)=>{
				var store = db.transaction(this._dbStoreName,'readwrite').objectStore(this._dbStoreName);
				store.delete(id).then(resolve).catch(reject);
			});
		});
	}

	private handleError(error: any) {
		return Promise.resolve(error.message || error);
	}

}