import { Pipe } from '@angular/core';

@Pipe({name: 'join'})
export class JoinPipe {
	transform(v: any, args: any[]){
		if(typeof v.join != "function") return v;
		return v.join(args ? args[0] : ', ');
	}
}

@Pipe({name:'hideEmpty'})
export class HideEmptyPipe {
	transform(v: any, args: any[]){
		if(typeof v == "undefined"||!v) return '';
		return v;
	}
}

@Pipe({name:'truncate'})
export class TruncatePipe {
	transform(v: any, length: number){
		if(typeof v == "undefined"||!v) return '';
		if(typeof v == "string"){
			var parts = v.split(' ');
			var string = "";
			var temp = "";
			for(var i = 0; i < parts.length; i++){
				temp += " "+parts[i];
				if(temp.length < length){
					string = temp;
				} else {
					return string.substr(1) + '...';
				}
			}
			return string.substr(1);
		}
		return v;
	}
}

@Pipe({name:'humanTime'})
export class HumanTimePipe {
	transform(input: any, args: any[]){
		var mins = parseInt(input);
		if (!isNaN(mins)) {

			var hours = Math.floor(mins / 60);
			if (hours > 0) {
				mins = (mins - (hours * 60));
			}
			var v = (hours ? hours + ' hr' + (hours > 1 ? 's' : '') + ' ' : '') + (mins > 0 ? mins + ' mins' : '');
			return v;
		} else {
			return '';
		}
	}
}

@Pipe({name:'linky'})
export class LinkyPipe {
	LINKY_URL_REGEXP =
	/((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"\u201d\u2019]/i;
	MAILTO_REGEXP = /^mailto:/i;

	transform(text: any, target, attributes){
		if (text == null || text === '' || !isString(text)) return text;

		var attributesFn =
			isFunction(attributes) ? attributes :
				isObject(attributes) ? function getAttributesObject() {return attributes;} :
					function getEmptyAttributesObject() {return {};};

		var match;
		var raw = text;
		var html = [];
		var url;
		var i;
		while ((match = raw.match(this.LINKY_URL_REGEXP))) {
			// We can not end in these as they are sometimes found at the end of the sentence
			url = match[0];
			// if we did not match ftp/http/www/mailto then assume mailto
			if (!match[2] && !match[4]) {
				url = (match[3] ? 'http://' : 'mailto:') + url;
			}
			i = match.index;
			addText(raw.substr(0, i));
			addLink(url, match[0].replace(this.MAILTO_REGEXP, ''));
			raw = raw.substring(i + match[0].length);
		}
		addText(raw);
		return html.join('');

		function isString(text){
			return typeof text == "string";
		}
		function isFunction(attributes){
			return typeof attributes == "function";
		}
		function isObject(object){
			return typeof object == "object";
		}
		function isDefined(defined){
			return typeof defined != "undefined";
		}

		function addText(text) {
			if (!text) {
				return;
			}
			html.push(text);
		}

		function addLink(url, text) {
			var key, linkAttributes = attributesFn(url);
			html.push('<a ');

			for (key in linkAttributes) {
				html.push(key + '="' + linkAttributes[key] + '" ');
			}

			if (isDefined(target) && !('target' in linkAttributes)) {
				html.push('target="',
					target,
					'" ');
			}
			html.push('href="',
				url.replace(/"/g, '&quot;'),
				'">');
			addText(text);
			html.push('</a>');
		}
	}
}

export const FORMATTING_PIPES: any = [
	JoinPipe,
	HideEmptyPipe,
	TruncatePipe,
	HumanTimePipe,
	LinkyPipe
];