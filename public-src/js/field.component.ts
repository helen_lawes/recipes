import { Component, Input, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';


@Component({
	selector:'inline-input',
	template:`
		<textarea *ngIf="type=='textarea'" [(ngModel)]="model" (ngModelChange)="modelChange($event)" [placeholder]="placeholder" (focus)="focus()" (blur)="blur()" [attr.id]="field" [required]="required"></textarea>
		<input *ngIf="type!='textarea'" [type]="type" [(ngModel)]="model" (ngModelChange)="modelChange($event)" [placeholder]="placeholder" (focus)="focus()" (blur)="blur()" [attr.id]="field" [required]="required" autocomplete="off">`
})

export class InlineInputComponent implements OnInit{

	@Input('type') type: string = 'text';
	@Input('model') model;
	@Input('label') label;
	@Input('field') field;
	@Input('required') required: boolean = false;
	@Input('placeholder') placeholder: string = '';
	@Output('focus') onFocus: EventEmitter<any> = new EventEmitter(false);
	@Output('blur') onBlur: EventEmitter<any> = new EventEmitter(false);
	@Output('modelChange') onModelChange: EventEmitter<any> = new EventEmitter(false);

	originalHeight = 0;

	constructor(private _el: ElementRef){}

	ngOnInit() {
		if(this.type=='textarea'){
			setTimeout(() =>{
				this.originalHeight = this.getHeight();
				this.setHeight(this.getHeight());
			},50);
		}
	}

	focus(){
		this.onFocus.emit(undefined);
	}
	blur(){
		this.onBlur.emit(undefined);
	}
	modelChange(){
		this.onModelChange.emit(this.model);
		if(this.type=='textarea'){
			this.setHeight(this.getHeight());
		}
	}

	private elementExists(){
		if(typeof this._el == "undefined" || !this._el) return false;
		else if( typeof this._el.nativeElement == "undefined") return false;
		else if( typeof this._el.nativeElement.firstElementChild == "undefined") return false;
		return true;
	}
	private getHeight(auto: boolean = true){
		if(!this.elementExists()) return;
		if(auto) this.setHeight('auto');
		return this._el.nativeElement.firstElementChild.scrollHeight + 2;
	}
	private setHeight(height){
		if(!this.elementExists() || !height) return;
		if(this.model){
			this._el.nativeElement.firstElementChild.style.height = height != 'auto' ? height +'px' : this.originalHeight+'px';
		} else {
			this._el.nativeElement.firstElementChild.style.height = false;
		}
	}

}

@Component({
	selector:'fancy-input',
	template:`
	<div class="row" [class.active]="active" [class.filled]="filled">
		<label *ngIf="label" [attr.for]="field">{{label}}</label>
		<inline-input [type]="type" [(model)]="model" [placeholder]="placeholder" (modelChange)="modelChange($event)" (focus)="focus()" (blur)="blur()" [field]="field" [required]="required" ></inline-input>
	</div>`
})

export class InputComponent implements OnInit{

	active: boolean;
	filled: boolean;
	@Input('type') type: string = 'text';
	@Input('model') model;
	@Input('label') label;
	@Input('field') field;
	@Input('required') required: boolean = false;
	@Input('placeholder') placeholder: string = '';
	@Output('modelChange') onModelChange: EventEmitter<any> = new EventEmitter(false);

	ngOnInit(){
		this.filled = this.model != null && this.model.length > 0;
	}
	focus(){
		this.active = true;
	}
	blur(){
		this.active = false;
		this.filled = this.model != null && this.model.length > 0;
	}
	modelChange($event){
		this.onModelChange.emit(this.model);
	}

}


export const FIELD_DIRECTIVES = [
	InlineInputComponent,
	InputComponent
];