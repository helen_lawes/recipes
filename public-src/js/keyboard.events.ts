import { Component, OnInit, Input, Output, ElementRef, Renderer } from '@angular/core';

export class KeyboardEvents{

	focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';

	constructor(){}

	triggerClick(e,func){
		let keyCode = e.keyCode;
		if(keyCode == 13 || keyCode == 32) {
			let args = [];
			for(var i = 2; i < arguments.length; i++){
				args.push(arguments[i]);
			}
			func.apply(this, args);
		}
	}

}