import { Component, OnInit, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RecipesService } from './recipes.service';
import { KeyboardEvents } from './keyboard.events';
import { SnackBarComponent } from './notification.component';

/**
 * Recipe list
 *
 * Displays all the users recipes
 */
@Component({
	selector:'recipes',
	templateUrl:'tpl/recipes.html'
})

export class RecipesComponent extends KeyboardEvents implements OnInit,AfterContentInit{
	_recipes: any;
	recipes: any;
	search: any;
	showSearch: boolean = false;
	category;
	snackOpen: boolean = false;

	@ViewChild('updateSnack') updateSnack: SnackBarComponent;
	@ViewChild('errorSnack') errorSnack: SnackBarComponent;

	private sub: any;

	constructor(private _route: ActivatedRoute,private _router: Router, private _recipeService: RecipesService){
		super();
	}

	ngOnInit(){
		this._recipeService.swEvents.subscribe(message=>{
			if(message=='ready'){
				this.updateSnack.show();
				this.snackOpen = true;
			}
		});
		this.search = sessionStorage.getItem('search');
		this.sub = this._route.params.subscribe(params => {
			this.category = params['category'];
			this._recipeService.getRecipes()
				.subscribe(recipes => {
					if(this.category){
						this._recipes = this.filter(recipes,this.category,'category');
					} else {
						this._recipes = recipes;
					}
					this._recipes.sort(this.recipeSort);
					this.recipes = this._recipes.slice(0);
					if (this.search && this.search != '') {
						this.search = '';
						this.searchUpdate();
						this.toggleSearch(true);
					}
				},
				e =>{
					this.errorSnack.show();
				});
		});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	ngAfterContentInit() {
		var scrollY = sessionStorage.getItem('recipes.scrollY')/1;
		setTimeout(function(){
			window.scrollTo(0,scrollY);
		},50);
	}

	viewRecipe(recipe){
		sessionStorage.setItem('recipes.scrollY',""+window.scrollY);
		if(this.category) sessionStorage.setItem('recipes.category',this.category);
		else sessionStorage.removeItem('recipes.category');
		this._router.navigate(['/recipes',recipe._id]);
	}

	createRecipe(){
		this._router.navigate(['/recipes/new']);
	}

	searchUpdate(){
		sessionStorage.setItem('search',this.search);
		if(this.search && this.search == ''){
			this.recipes = this._recipes.slice(0);
		} else {
			this.recipes = this.filter(this._recipes,this.search,'name');
		}
	}

	filter(toFilter,search,property){
		let recipes = toFilter.slice(0);
		recipes = recipes.filter(obj => {
			var regex = new RegExp(search,'i');
			return !!regex.test(obj[property]);
		});
		return recipes;
	}

	recipeSort(a,b){
		let nameA = a.name.toUpperCase();
		let nameB = b.name.toUpperCase();
		if( nameA < nameB ){
			return -1;
		}
		if( nameA > nameB ){
			return 1;
		}

		return 0;
	}

	resetSearch(){
		this.search = '';
		this.toggleSearch(false);
		this.searchUpdate();
	}

	toggleSearch(value?){
		this.showSearch = value || !this.showSearch;
	}

	installUpdate(){
		this.snackOpen = false;
		this._recipeService.swUpdateConfirmed();
	}
}