import { Component, Input, Output, EventEmitter, AfterContentInit, ElementRef } from '@angular/core';

/**
 * Modal container
 *
 * Example:
 * <modal (open)="openHandler()" (confirm)="confirmHandler()" (close)="closeHandler()">
 *     Modal contents
 *     <modal-footer>Footer contents</modal-footer>
 * </modal>
 */
@Component({
	selector:'modal',
	template:`<div class="modal" [class.shown]="visible" role="dialog" (keydown)="trapKey($event)">
			<div class="modal-content">
				<ng-content></ng-content>
			</div>
		</div>`
})

export class ModalComponent implements AfterContentInit{

	visible: boolean = false;
	firstTabStop;
	lastTabStop;
	previousActive;

	// output event onOpen bound with (open)="myHandler()"
	@Output('open') onOpen: EventEmitter<any> = new EventEmitter(false);
	// output event onConfirm bound with (confirm)="myHandler()"
	@Output('confirm') onConfirm: EventEmitter<any> = new EventEmitter(false);
	// output event onClose bound with (close)="myHandler()"
	@Output('close') onClose: EventEmitter<any> = new EventEmitter(false);

	constructor(private _modal: ElementRef){}

	ngAfterContentInit(){
		let focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';

		setTimeout(()=>{
			let focusableElements = this._modal.nativeElement.querySelectorAll(focusableElementsString);
			focusableElements = Array.prototype.slice.call(focusableElements);
			this.firstTabStop = focusableElements[0];
			this.lastTabStop = focusableElements[focusableElements.length - 1];
		},50);
	}

	open() {
		this.visible = true;
		this.previousActive = document.activeElement;
		this.onOpen.emit(undefined);
		setTimeout(()=>{
			this.firstTabStop.focus();
		},50);
	}

	close(cancel?:boolean) {
		this.visible = false;
		if(cancel){
			this.onClose.emit(undefined);
		} else {
			this.onConfirm.emit(undefined);
		}
		this.previousActive.focus();
	}
	trapKey(e) {
		// Check for TAB key press
		if (e.keyCode === 9) {

			// SHIFT + TAB
			if (e.shiftKey) {
				if (document.activeElement === this.firstTabStop) {
					e.preventDefault();
					this.lastTabStop.focus();
				}

				// TAB
			} else {
				if (document.activeElement === this.lastTabStop) {
					e.preventDefault();
					this.firstTabStop.focus();
				}
			}
		}

		// ESCAPE
		if (e.keyCode === 27) {
			this.close(true);
		}
	}


}

/**
 * Modal footer
 *
 * Example:
 * <modal-footer [show-default-buttons]="true"></modal-footer>
 * <modal-footer>Footer contents</modal-footer>
 */
@Component({
	selector:'modal-footer',
	template:`<div class="modal-footer">
			<ng-content></ng-content>
			<button *ngIf="showDefaultButtons" (click)="cancel()">{{cancelButton}}</button>
			<button *ngIf="showDefaultButtons" (click)="confirm()">{{confirmButton}}</button>
		</div>`
})

export class ModalFooterComponent{

	@Input('show-default-buttons') showDefaultButtons: boolean = false;
	@Input('cancel-button') cancelButton: string = 'Cancel';
	@Input('confirm-button') confirmButton: string = 'OK';

	constructor(private _modal: ModalComponent){};

	confirm(){
		this._modal.close();
	}
	cancel(){
		this._modal.close(true);
	}

}

export const MODAL_DIRECTIVES = [
	ModalComponent,
	ModalFooterComponent
];