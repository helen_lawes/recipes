import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';

/**
 * Login
 *
 * Handles logging in of the user and storing the json web token to local storage
 */
@Component({
	selector:'login',
	templateUrl:'tpl/login.html'
})

export class LoginComponent implements OnInit{
	login: boolean = true;
	errorMessage: string = '';
	email: string = '';
	emailConfirm: string = '';
	password: string = '';
	passwordConfirm: string = '';
	url;
	loading: boolean = false;

	constructor(private _route: ActivatedRoute,private _router: Router,private _userService: UserService){}

	ngOnInit(){
		this.url = this._route.url.subscribe(url =>{
			this.login = url[0].path != 'register';
		});
	}

	toggleForm(form){
		this._reset();
		this.loading = false;
		this.login = (form=='login');
	}

	private _reset(){
		this.errorMessage = '';
	}

	submitForm(){
		this._reset();
		this.loading = true;
		if(this.login) this.userLogin();
		else this.userRegister();
	}

	userLogin(){
		let error = this._validate();
		// check login credentials with the user service
		if(!error)
			this._userService.login(this.email,this.password)
				.then(response => this.handleResponse(response));
		else this.loading = false;
	}

	userRegister(){
		// send registration details with the user service
		let user = {
			email: this.email,
			emailConfirm: this.emailConfirm,
			password: this.password,
			passwordConfirm: this.passwordConfirm
		};

		let error = this._validate();

		if(!error)
			this._userService.register(user)
				.then(response => this.handleResponse(response));
		else this.loading = false;
	}

	private _validate(){

		let error;
		let additional = !this.login ? `and confirm` : ``;
		if( this.email == "" ){
			this.errorMessage += `Please fill in email ${additional}\n`;
			error = true;
		}

		if( this.password == "" ){
			this.errorMessage += `Please fill in password ${additional}\n`;
			error = true;
		}

		if(this.login) return error;

		if( this.email != this.emailConfirm ){
			this.errorMessage += `Email and confirm don't match\n`;
			error = true;
		}

		if( this.password != this.passwordConfirm ){
			this.errorMessage += `Password and confirm don't match\n`;
			error = true;
		}

		return error;
	}

	private storeToken(token){
		window.localStorage.setItem('token',token);
	}

	private handleResponse(response){
		this.loading = false;
		if(typeof response.status != "undefined"){
			// print to screen any error message sent back from the API
			this.errorMessage = response._body;
		} else {
			if(this.login){
				// store the jwt after successful login and redirect to recipe list
				this.storeToken(response);
				this._router.navigate(['/recipes']);
			} else {
				this.login = true;
			}
		}
	}

}