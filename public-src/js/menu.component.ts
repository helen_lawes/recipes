import { Component, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { UserService } from './user.service';
import { Router } from '@angular/router';

@Component({
	selector:'menu',
	templateUrl:'tpl/menu.component.html'
})

export class MenuComponent{

	constructor(private _userService: UserService, private _router: Router){}

	@Input('side-menu') showMenu: boolean = false;
	showSideMenu: boolean = false;
	firstTabStop;
	lastTabStop;
	previousActive;


	toggleSideMenu(){
		this.showSideMenu = !this.showSideMenu;
		if(this.showSideMenu){
			this.opened();
		} else {
			this.closed();
		}
	}

	opened(){
		let focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';
		let modal = document.querySelector('.side-menu');
		let focusableElements = modal.querySelectorAll(focusableElementsString);
		focusableElements = Array.prototype.slice.call(focusableElements);
		this.firstTabStop = focusableElements[0];
		this.lastTabStop = focusableElements[focusableElements.length - 1];
		this.previousActive = document.activeElement;
		setTimeout(()=>{
			this.firstTabStop.focus();
		},50);
	}
	closed(){
		this.previousActive.focus();
	}

	logout(){
		this._userService.logout();
		this._router.navigate(['/login'],{ queryParams: { }});
	}

	goToCategory(category){
		this._reset();
		this._router.navigate(['/recipes/categories',category]);
	}

	goToAll(){
		this._reset();
		this._router.navigate(['/recipes']);
	}

	trapKey(e) {
		// Check for TAB key press
		if (e.keyCode === 9) {

			// SHIFT + TAB
			if (e.shiftKey) {
				if (document.activeElement === this.firstTabStop) {
					e.preventDefault();
					this.lastTabStop.focus();
				}

				// TAB
			} else {
				if (document.activeElement === this.lastTabStop) {
					e.preventDefault();
					this.firstTabStop.focus();
				}
			}
		}

		// ESCAPE
		if (e.keyCode === 27) {
			this.toggleSideMenu();
		}
	}

	private _reset(){
		this.toggleSideMenu();
		sessionStorage.setItem('recipes.scrollY',"0");
	}

}

export const MENU_DIRECTIVES = [
	MenuComponent
];