import { Injectable } from '@angular/core';

export class DatabaseObjectStore{

	constructor(private _store){

		let funcs = ['add','clear','delete','get','getAll','getAllKeys','put'];

		funcs.forEach((func)=>{
			this[func] = (arg1,arg2) => {
				return new Promise((resolve,reject)=>{
					let action = this._store[func](arg1,arg2);
					action.onsuccess = (event) =>{
						resolve(action.result);
					};
					action.onerror = (evt) =>{
						reject(evt);
					};
				});
			}
		})

	}

	loopCursor(loopFunc){
		return new Promise((resolve,reject) =>{
			let action = this._store.openCursor();
			action.onsuccess = (event) =>{
				var cursor = event.target.result;
				if(!cursor){
					resolve();
					return;
				}
				loopFunc(cursor);
				cursor.continue();
			};
			action.onerror = (e)=>{
				reject(e);
			}
		});
	}

}

export class DatabaseTransaction{

	store;

	constructor(private _tx){}

	objectStore(storeName){
		this.store = this._tx.objectStore(storeName);
		return new DatabaseObjectStore(this.store);
	}

}
export class DatabaseConnection{

	tx;

	constructor(private _dbRes){}

	transaction(storeName,mode = 'readonly'){
		this.tx = this._dbRes.transaction(storeName,mode);
		return new DatabaseTransaction(this.tx);
	}

}


@Injectable()
export class Database{

	request;
	db;

	open(name,version,upgraded){

		return new Promise((resolve,reject)=>{

			this.request = window.indexedDB.open(name,version);

			this.request.onupgradeneeded = (event) => {
				this.db = event.target.result;
				if(typeof upgraded == "function"){
					upgraded(this.db);
				}
				resolve(new DatabaseConnection(this.db));
			};

			this.request.onsuccess = (event) => {
				this.db = event.target.result;
				resolve(new DatabaseConnection(this.db));
			};

			this.request.onerror = () => {
				reject();
			};
		});

	}

}