import { Injectable } from '@angular/core';

export class ObjectId{

	increment = Math.floor(Math.random() * (16777216));
	pid = Math.floor(Math.random() * (65536));
	machine = Math.floor(Math.random() * (16777216));
	timestamp;

	constructor(){
		this.checkStorage();

		if (typeof (arguments[0]) == 'object') {
			this.timestamp = arguments[0].timestamp;
			this.machine = arguments[0].machine;
			this.pid = arguments[0].pid;
			this.increment = arguments[0].increment;
		}
		else if (typeof (arguments[0]) == 'string' && arguments[0].length == 24) {
			this.timestamp = Number('0x' + arguments[0].substr(0, 8)),
				this.machine = Number('0x' + arguments[0].substr(8, 6)),
				this.pid = Number('0x' + arguments[0].substr(14, 4)),
				this.increment = Number('0x' + arguments[0].substr(18, 6))
		}
		else if (arguments.length == 4 && arguments[0] != null) {
			this.timestamp = arguments[0];
			this.machine = arguments[1];
			this.pid = arguments[2];
			this.increment = arguments[3];
		}
		else {
			this.timestamp = Math.floor(new Date().valueOf() / 1000);
			this.increment++;
			if (this.increment > 0xffffff) {
				this.increment = 0;
			}
		}
	}

	setMachineCookie() {
		var cookieList = document.cookie.split('; ');
		for (var i in cookieList) {
			var cookie = cookieList[i].split('=');
			var cookieMachineId = parseInt(cookie[1], 10);
			if (cookie[0] == 'mongoMachineId' && cookieMachineId && cookieMachineId >= 0 && cookieMachineId <= 16777215) {
				this.machine = cookieMachineId;
				break;
			}
		}
		document.cookie = 'mongoMachineId=' + this.machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT;path=/';
	}

	checkStorage(){

		if (typeof (localStorage) != 'undefined') {
			try {
				var mongoMachineId = parseInt(localStorage['mongoMachineId']);
				if (mongoMachineId >= 0 && mongoMachineId <= 16777215) {
					this.machine = Math.floor(localStorage['mongoMachineId']);
				}
				// Just always stick the value in.
				localStorage['mongoMachineId'] = this.machine;
			} catch (e) {
				this.setMachineCookie();
			}
		}
		else {
			this.setMachineCookie();
		}
	}

	getDate() {
		return new Date(this.timestamp * 1000);
	}

	toArray() {
		var strOid = this.toString();
		var array = [];
		var i;
		for(i = 0; i < 12; i++) {
			array[i] = parseInt(strOid.slice(i*2, i*2+2), 16);
		}
		return array;
	}

	toString() {
		if (this.timestamp === undefined
			|| this.machine === undefined
			|| this.pid === undefined
			|| this.increment === undefined) {
			return 'Invalid ObjectId';
		}

		var timestamp = this.timestamp.toString(16);
		var machine = this.machine.toString(16);
		var pid = this.pid.toString(16);
		var increment = this.increment.toString(16);
		return '00000000'.substr(0, 8 - timestamp.length) + timestamp +
			'000000'.substr(0, 6 - machine.length) + machine +
			'0000'.substr(0, 4 - pid.length) + pid +
			'000000'.substr(0, 6 - increment.length) + increment;
	}
}