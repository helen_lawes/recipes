import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RecipesService } from './recipes.service';
import { ModalComponent } from "./modal.component";
import { SnackBarComponent } from './notification.component';

/**
 * Recipe edit
 *
 * This is used to edit existing recipes as well as adding new recipes
 */
@Component({
	selector:'recipe-edit',
	templateUrl:'tpl/recipe-edit.html'
})

export class RecipeEditComponent implements OnInit,OnDestroy{
	//get the view attached to the variable deleteModal to trigger component events
	@ViewChild('discardModal') discardModal: ModalComponent;
	@ViewChild('errorSnack') errorSnack: SnackBarComponent;
	recipe: any;
	mode: string = 'edit';

	private sub: any;

	constructor(private _route: ActivatedRoute, private _router: Router,private _recipeService: RecipesService){}

	ngOnInit(){
		window.scrollTo(0,0);
		// check the route params for an id, if there is one then retrieve recipe details
		// otherwise retrieve blank properties for a new recipe
		this.sub = this._route.params.subscribe(params =>{
			let id = params['id'];

			if(id){
				this._recipeService.getRecipe(id)
					.subscribe(recipe => this.recipe = this.deepCopy(recipe),
						e =>{
							this.errorSnack.show();
						});
			} else {
				this.mode = 'new';
				this._recipeService.getBlank()
					.then(recipe => {
						this.recipe = this.deepCopy(recipe)
					});
			}
		});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	goBack(saved:boolean = false){
		let id = this.recipe._id ? this.recipe._id : null;
		if(this.mode == 'edit'){
			if(saved){
				this._router.navigate(['/recipes',id]);
			} else {
				this._recipeService.getRecipe(id)
					.subscribe(recipe => {
						let changed = this.compare(this.recipe,recipe);
						if(changed){
							this.discardModal.open();
						} else {
							this._router.navigate(['/recipes',id]);
						}
					});
			}
		} else {
			this._router.navigate(['/recipes']);
		}
	}

	discardChanges(){
		let id = this.recipe._id;
		this._router.navigate(['/recipes',id]);
	}

	save(){
		if(typeof this.recipe.tags.split != "undefined") {
			this.recipe.tags = this.recipe.tags.split(',');
			for(let i = 0; i < this.recipe.tags.length; i++){
				this.recipe.tags[i] = this.recipe.tags[i].trim();
			}
		}
		if(this.mode == 'edit'){
			this._recipeService.updateRecipe(this.recipe)
				.then(()=> {
					this.goBack(true)
				});

		} else {
			this._recipeService.createRecipe(this.recipe)
				.then((resp: {_id})=>{
					if(resp._id){
						this._router.navigate(['/recipes',resp._id]);
					}
				})
		}
	}

	addIngredient(){
		this.recipe.ingredients.push({});
	}

	deleteIngredient(ingredient){
		this.deleteFrom(this.recipe.ingredients,ingredient);
	}

	addMethod(){
		if(typeof this.recipe.methods == "undefined") this.recipe.methods = [];
		this.recipe.methods.push({});
	}

	deleteMethod(method){
		this.deleteFrom(this.recipe.methods,method);
	}

	addTag(){
		this.recipe.tags.push('');
	}

	deleteTag(tag){
		this.deleteFrom(this.recipe.tags,tag);
	}

	fileChange(event){
		var file = event.target.files[0];
		var filename = file.name.split('.');
		if(this.recipe._id){
			this.recipe.photo = this.recipe._id +'.'+ filename[filename.length-1] + '?' + new Date().getTime();
		}
		this.recipe.file = file;
	}

	private deleteFrom(variable,toFind){
		var index = variable.indexOf(toFind);
		if(index > -1){
			variable.splice(index,1);
		}
	}

	private deepCopy(obj){
		var newObj;
		if(Object.prototype.toString.call(obj) === '[object Array]'){
			newObj = [];
			for(let item of obj){
				if(typeof item == "object"){
					newObj.push(this.deepCopy(item));
				} else {
					newObj.push(item);
				}
			}
		} else {
			newObj = Object.assign({},obj);
			for(let key in newObj){
				if(typeof newObj[key] == "object"){
					newObj[key] = this.deepCopy(newObj[key]);
				}
			}
		}
		return newObj
	}

	private compare(obj1,obj2){
		let diff = false;
		for( let key in obj1){
			if(typeof obj2[key] == "undefined"){
				diff = true;
			} else if( typeof obj1[key] != typeof obj2[key] ){
				diff = true;
			} else if( typeof obj1[key] == "object" ){
				if(this.compare(obj1[key],obj2[key])){
					diff = true;
				}
			} else if( obj1[key] != obj2[key] ){
				diff = true;
			}
		}
		return diff;
	}
}