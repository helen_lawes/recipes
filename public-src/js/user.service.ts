import { Injectable }    from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

/**
 * User service
 *
 * Interacts with the backend API
 */
@Injectable()
export class UserService{

	private baseUrl: string = '/api/';

	constructor(private _http: Http){}

	login(email,password){
		let headers = new Headers({
			'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		let user = {
			email,
			password
		};

		return this._http.post(`${this.baseUrl}login`,JSON.stringify(user),options)
			.toPromise()
			.then(response => response.json().token)
			.catch(this.handleError);
	}

	register(user){
		let headers = new Headers({
			'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});


		return this._http.post(`${this.baseUrl}register`,JSON.stringify(user),options)
			.toPromise()
			.then(response => response.json().result)
			.catch(this.handleError);
	}

	logout(){
		window.localStorage.removeItem('token');
	}

	isLoggedIn(){
		return window.localStorage.getItem('token') || false;
	}

	private handleError(error: any) {
		return Promise.resolve(error.message || error);
	}
}