import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AppComponent } from './app.component';
import { Router, ActivatedRoute } from '@angular/router';
import { RecipesService } from './recipes.service';
import { ModalComponent } from "./modal.component";
import { SnackBarComponent } from './notification.component';


/**
 * Recipe detail
 *
 * Displays all the information relating to a particular recipe
 */
@Component({
	selector:'recipe-detail',
	templateUrl:'tpl/recipe-detail.html'
})

export class RecipeDetailComponent implements OnInit,OnDestroy{
	//get the view attached to the variable deleteModal to trigger component events
	@ViewChild('deleteModal') deleteModal: ModalComponent;
	@ViewChild('errorSnack') errorSnack: SnackBarComponent;
	recipe: any;
	showFab: boolean = false;

	private sub: any;

	constructor(private _route: ActivatedRoute, private _router: Router,private _recipeService: RecipesService){}

	ngOnInit(){
		window.scrollTo(0,0);
		// get the id from the route params and request recipe from recipe service
		this.sub = this._route.params.subscribe(params =>{
			let id = params['id'];
			this._recipeService.getRecipe(id)
				.subscribe(recipe => this.recipe = recipe,
					e =>{
						this.errorSnack.show();
					});
		});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	goToRecipes(){
		let category = sessionStorage.getItem('recipes.category');
		if(typeof category != "undefined" && category){
			this._router.navigate(['/recipes/categories',category]);
		} else {
			this._router.navigate(['/recipes']);
		}
	}

	editRecipe(){
		// go to recipe edit route
		if(!this.recipe) return;
		this._router.navigate(['/recipes',this.recipe._id,'edit']);
	}

	deleteRecipe(){
		if(!this.recipe) return;
		// open confirmation dialog
		this.deleteModal.open();
	}

	confirmRecipeDelete(){
		if(!this.recipe) return;
		// delete recipe and then redirect back to the recipe listing
		this._recipeService.deleteRecipe(this.recipe)
			.then(() => {
				this.goToRecipes();
			});

	}

	toggleFab(){
		this.showFab = !this.showFab;
	}

}