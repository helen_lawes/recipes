import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({
	selector:'snackbar',
	template:`<div class="snackbar" [class.shown]="shown">
		<div class="snackbar-content"><ng-content></ng-content></div>
		<div class="snackbar-button" *ngIf="button" (click)="action()">{{button}}</div>
	</div>`
})

export class SnackBarComponent{

	shown: boolean = false;

	@Input('button') button = false;
	@Output('action') onAction: EventEmitter<any> = new EventEmitter(false);

	show(){
		this.shown = true;
	}

	hide(){
		this.shown = false;
	}

	action(){
		this.hide();
		this.onAction.emit(undefined);
	}
}

export const NOTIFICATION_DIRECTIVES = [
	SnackBarComponent
];