import { platformBrowserDynamic  } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

// setup main app
platformBrowserDynamic().bootstrapModule(AppModule);