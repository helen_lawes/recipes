import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserService } from './user.service';

/**
 * AuthGuard
 *
 * check if user is logged in (has json web token) otherwise redirect to the login page
 */
@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private _userService: UserService, private _router: Router) {}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (this._userService.isLoggedIn()) {
			return true;
		}

		this._router.navigate(['/login'],{ queryParams: { }});
		return false;
	}
}