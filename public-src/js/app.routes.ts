import { Routes, RouterModule } from '@angular/router';

import { RecipesComponent } from './recipes.component';
import { LoginComponent } from './login.component';
import { AuthGuard } from './auth.guard';
import { UserService } from './user.service';
import { RecipeDetailComponent } from './recipe-detail.component';
import { RecipeEditComponent } from './recipe-edit.component';
import { RecipesService } from './recipes.service';
import { SocketService } from './socket.service';
import { Database } from './database';

/**
 * Router configuration
 *
 * routes with canActivate: [AuthGuard] require a valid json web token to access
 */
export const routes: Routes = [
	{ path: '', redirectTo:'/login', pathMatch:'full'},
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: LoginComponent },
	{ path: 'recipes', component: RecipesComponent, canActivate: [AuthGuard] },
	{ path: 'recipes/categories/:category', component: RecipesComponent, canActivate: [AuthGuard] },
	{ path: 'recipes/new', component: RecipeEditComponent, canActivate: [AuthGuard] },
	{ path: 'recipes/:id', component: RecipeDetailComponent, canActivate: [AuthGuard] },
	{ path: 'recipes/:id/edit', component: RecipeEditComponent, canActivate: [AuthGuard] }
];

// require route provider along with AuthGuard, UserService and RecipesService as these are
// used throughout the app and only one global instance is created
export const appRoutingProviders: any[] = [
	AuthGuard,
	UserService,
	RecipesService,
	SocketService,
	Database
];

export const routing = RouterModule.forRoot(routes);

export const routeComponents = [
	RecipesComponent,
	LoginComponent,
	RecipeDetailComponent,
	RecipeEditComponent
];