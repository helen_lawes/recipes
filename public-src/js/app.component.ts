import { Component } from '@angular/core';

/**
 * Main angular app
 *
 * No inputs required
 */
@Component({
	selector:'my-app',
	template:'<router-outlet></router-outlet>'
})

export class AppComponent {}