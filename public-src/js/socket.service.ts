declare var io;
import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SocketService{

	private _socket;
	private _connection: EventEmitter<any> = new EventEmitter();

	constructor(){
		if(io){
			let path = window.location.protocol + '//' + window.location.hostname + ':8080';
			this._socket = io.connect(path);
			this._socket.on('connect',()=>{
				this._connection.emit(true);
			});
			this._socket.on('disconnect',()=>{
				this._connection.emit(false);
			});
			this._socket.on('error',(e)=>{
				// TODO: intercept errors
			});
			this._socket.on('message',(message)=>{
				// TODO: intercept messages
			});
		}
	}

	getConnection(){
		return this._connection;
	}

}

