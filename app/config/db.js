module.exports = {
	host: process.env.MONGO_NODE_DRIVER_HOST || 'localhost',
	port: process.env.MONGO_NODE_DRIVER_PORT || 27017,
	name: 'Recipes',
	collections: {
		recipes: 'recipes',
		users: 'users',
		list: 'list'
	}
};
