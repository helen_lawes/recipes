let MongoClient = require('mongodb').MongoClient,
    MDB = require('../config/db');

let connection = `mongodb://${MDB.host}:${MDB.port}/${MDB.name}`;
console.log(`Database connection: ${connection}`);

let db;

MongoClient.connect(connection)
    .then((database)=>{
        db = database;
        console.log("Connected to database");
        let promises = [];
        for( let name in MDB.collections ){
            promises.push(db.createCollection(`${MDB.collections[name]}`));
            console.log(`Added collection: ${MDB.collections[name]}`);
        }
        return Promise.all(promises);
    })
    .then((results)=>{
        console.log("Created all collections");
        db.close();
        console.log("All operations completed without error.");
    })
    .catch(e=>{
        console.error(e);
    });
