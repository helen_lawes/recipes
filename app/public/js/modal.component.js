"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
/**
 * Modal container
 *
 * Example:
 * <modal (open)="openHandler()" (confirm)="confirmHandler()" (close)="closeHandler()">
 *     Modal contents
 *     <modal-footer>Footer contents</modal-footer>
 * </modal>
 */
var ModalComponent = (function () {
    function ModalComponent(_modal) {
        this._modal = _modal;
        this.visible = false;
        // output event onOpen bound with (open)="myHandler()"
        this.onOpen = new core_1.EventEmitter(false);
        // output event onConfirm bound with (confirm)="myHandler()"
        this.onConfirm = new core_1.EventEmitter(false);
        // output event onClose bound with (close)="myHandler()"
        this.onClose = new core_1.EventEmitter(false);
    }
    ModalComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        var focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';
        setTimeout(function () {
            var focusableElements = _this._modal.nativeElement.querySelectorAll(focusableElementsString);
            focusableElements = Array.prototype.slice.call(focusableElements);
            _this.firstTabStop = focusableElements[0];
            _this.lastTabStop = focusableElements[focusableElements.length - 1];
        }, 50);
    };
    ModalComponent.prototype.open = function () {
        var _this = this;
        this.visible = true;
        this.previousActive = document.activeElement;
        this.onOpen.emit(undefined);
        setTimeout(function () {
            _this.firstTabStop.focus();
        }, 50);
    };
    ModalComponent.prototype.close = function (cancel) {
        this.visible = false;
        if (cancel) {
            this.onClose.emit(undefined);
        }
        else {
            this.onConfirm.emit(undefined);
        }
        this.previousActive.focus();
    };
    ModalComponent.prototype.trapKey = function (e) {
        // Check for TAB key press
        if (e.keyCode === 9) {
            // SHIFT + TAB
            if (e.shiftKey) {
                if (document.activeElement === this.firstTabStop) {
                    e.preventDefault();
                    this.lastTabStop.focus();
                }
            }
            else {
                if (document.activeElement === this.lastTabStop) {
                    e.preventDefault();
                    this.firstTabStop.focus();
                }
            }
        }
        // ESCAPE
        if (e.keyCode === 27) {
            this.close(true);
        }
    };
    __decorate([
        core_1.Output('open'), 
        __metadata('design:type', core_1.EventEmitter)
    ], ModalComponent.prototype, "onOpen", void 0);
    __decorate([
        core_1.Output('confirm'), 
        __metadata('design:type', core_1.EventEmitter)
    ], ModalComponent.prototype, "onConfirm", void 0);
    __decorate([
        core_1.Output('close'), 
        __metadata('design:type', core_1.EventEmitter)
    ], ModalComponent.prototype, "onClose", void 0);
    ModalComponent = __decorate([
        core_1.Component({
            selector: 'modal',
            template: "<div class=\"modal\" [class.shown]=\"visible\" role=\"dialog\" (keydown)=\"trapKey($event)\">\n\t\t\t<div class=\"modal-content\">\n\t\t\t\t<ng-content></ng-content>\n\t\t\t</div>\n\t\t</div>"
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], ModalComponent);
    return ModalComponent;
}());
exports.ModalComponent = ModalComponent;
/**
 * Modal footer
 *
 * Example:
 * <modal-footer [show-default-buttons]="true"></modal-footer>
 * <modal-footer>Footer contents</modal-footer>
 */
var ModalFooterComponent = (function () {
    function ModalFooterComponent(_modal) {
        this._modal = _modal;
        this.showDefaultButtons = false;
        this.cancelButton = 'Cancel';
        this.confirmButton = 'OK';
    }
    ;
    ModalFooterComponent.prototype.confirm = function () {
        this._modal.close();
    };
    ModalFooterComponent.prototype.cancel = function () {
        this._modal.close(true);
    };
    __decorate([
        core_1.Input('show-default-buttons'), 
        __metadata('design:type', Boolean)
    ], ModalFooterComponent.prototype, "showDefaultButtons", void 0);
    __decorate([
        core_1.Input('cancel-button'), 
        __metadata('design:type', String)
    ], ModalFooterComponent.prototype, "cancelButton", void 0);
    __decorate([
        core_1.Input('confirm-button'), 
        __metadata('design:type', String)
    ], ModalFooterComponent.prototype, "confirmButton", void 0);
    ModalFooterComponent = __decorate([
        core_1.Component({
            selector: 'modal-footer',
            template: "<div class=\"modal-footer\">\n\t\t\t<ng-content></ng-content>\n\t\t\t<button *ngIf=\"showDefaultButtons\" (click)=\"cancel()\">{{cancelButton}}</button>\n\t\t\t<button *ngIf=\"showDefaultButtons\" (click)=\"confirm()\">{{confirmButton}}</button>\n\t\t</div>"
        }), 
        __metadata('design:paramtypes', [ModalComponent])
    ], ModalFooterComponent);
    return ModalFooterComponent;
}());
exports.ModalFooterComponent = ModalFooterComponent;
exports.MODAL_DIRECTIVES = [
    ModalComponent,
    ModalFooterComponent
];
