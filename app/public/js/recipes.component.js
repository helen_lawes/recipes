"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var recipes_service_1 = require('./recipes.service');
var keyboard_events_1 = require('./keyboard.events');
var notification_component_1 = require('./notification.component');
/**
 * Recipe list
 *
 * Displays all the users recipes
 */
var RecipesComponent = (function (_super) {
    __extends(RecipesComponent, _super);
    function RecipesComponent(_route, _router, _recipeService) {
        _super.call(this);
        this._route = _route;
        this._router = _router;
        this._recipeService = _recipeService;
        this.showSearch = false;
        this.snackOpen = false;
    }
    RecipesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._recipeService.swEvents.subscribe(function (message) {
            if (message == 'ready') {
                _this.updateSnack.show();
                _this.snackOpen = true;
            }
        });
        this.search = sessionStorage.getItem('search');
        this.sub = this._route.params.subscribe(function (params) {
            _this.category = params['category'];
            _this._recipeService.getRecipes()
                .subscribe(function (recipes) {
                if (_this.category) {
                    _this._recipes = _this.filter(recipes, _this.category, 'category');
                }
                else {
                    _this._recipes = recipes;
                }
                _this._recipes.sort(_this.recipeSort);
                _this.recipes = _this._recipes.slice(0);
                if (_this.search && _this.search != '') {
                    _this.search = '';
                    _this.searchUpdate();
                    _this.toggleSearch(true);
                }
            }, function (e) {
                _this.errorSnack.show();
            });
        });
    };
    RecipesComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    RecipesComponent.prototype.ngAfterContentInit = function () {
        var scrollY = sessionStorage.getItem('recipes.scrollY') / 1;
        setTimeout(function () {
            window.scrollTo(0, scrollY);
        }, 50);
    };
    RecipesComponent.prototype.viewRecipe = function (recipe) {
        sessionStorage.setItem('recipes.scrollY', "" + window.scrollY);
        if (this.category)
            sessionStorage.setItem('recipes.category', this.category);
        else
            sessionStorage.removeItem('recipes.category');
        this._router.navigate(['/recipes', recipe._id]);
    };
    RecipesComponent.prototype.createRecipe = function () {
        this._router.navigate(['/recipes/new']);
    };
    RecipesComponent.prototype.searchUpdate = function () {
        sessionStorage.setItem('search', this.search);
        if (this.search && this.search == '') {
            this.recipes = this._recipes.slice(0);
        }
        else {
            this.recipes = this.filter(this._recipes, this.search, 'name');
        }
    };
    RecipesComponent.prototype.filter = function (toFilter, search, property) {
        var recipes = toFilter.slice(0);
        recipes = recipes.filter(function (obj) {
            var regex = new RegExp(search, 'i');
            return !!regex.test(obj[property]);
        });
        return recipes;
    };
    RecipesComponent.prototype.recipeSort = function (a, b) {
        var nameA = a.name.toUpperCase();
        var nameB = b.name.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    };
    RecipesComponent.prototype.resetSearch = function () {
        this.search = '';
        this.toggleSearch(false);
        this.searchUpdate();
    };
    RecipesComponent.prototype.toggleSearch = function (value) {
        this.showSearch = value || !this.showSearch;
    };
    RecipesComponent.prototype.installUpdate = function () {
        this.snackOpen = false;
        this._recipeService.swUpdateConfirmed();
    };
    __decorate([
        core_1.ViewChild('updateSnack'), 
        __metadata('design:type', notification_component_1.SnackBarComponent)
    ], RecipesComponent.prototype, "updateSnack", void 0);
    __decorate([
        core_1.ViewChild('errorSnack'), 
        __metadata('design:type', notification_component_1.SnackBarComponent)
    ], RecipesComponent.prototype, "errorSnack", void 0);
    RecipesComponent = __decorate([
        core_1.Component({
            selector: 'recipes',
            templateUrl: 'tpl/recipes.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, recipes_service_1.RecipesService])
    ], RecipesComponent);
    return RecipesComponent;
}(keyboard_events_1.KeyboardEvents));
exports.RecipesComponent = RecipesComponent;
