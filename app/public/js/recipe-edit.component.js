"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var recipes_service_1 = require('./recipes.service');
var modal_component_1 = require("./modal.component");
var notification_component_1 = require('./notification.component');
/**
 * Recipe edit
 *
 * This is used to edit existing recipes as well as adding new recipes
 */
var RecipeEditComponent = (function () {
    function RecipeEditComponent(_route, _router, _recipeService) {
        this._route = _route;
        this._router = _router;
        this._recipeService = _recipeService;
        this.mode = 'edit';
    }
    RecipeEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        window.scrollTo(0, 0);
        // check the route params for an id, if there is one then retrieve recipe details
        // otherwise retrieve blank properties for a new recipe
        this.sub = this._route.params.subscribe(function (params) {
            var id = params['id'];
            if (id) {
                _this._recipeService.getRecipe(id)
                    .subscribe(function (recipe) { return _this.recipe = _this.deepCopy(recipe); }, function (e) {
                    _this.errorSnack.show();
                });
            }
            else {
                _this.mode = 'new';
                _this._recipeService.getBlank()
                    .then(function (recipe) {
                    _this.recipe = _this.deepCopy(recipe);
                });
            }
        });
    };
    RecipeEditComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    RecipeEditComponent.prototype.goBack = function (saved) {
        var _this = this;
        if (saved === void 0) { saved = false; }
        var id = this.recipe._id ? this.recipe._id : null;
        if (this.mode == 'edit') {
            if (saved) {
                this._router.navigate(['/recipes', id]);
            }
            else {
                this._recipeService.getRecipe(id)
                    .subscribe(function (recipe) {
                    var changed = _this.compare(_this.recipe, recipe);
                    if (changed) {
                        _this.discardModal.open();
                    }
                    else {
                        _this._router.navigate(['/recipes', id]);
                    }
                });
            }
        }
        else {
            this._router.navigate(['/recipes']);
        }
    };
    RecipeEditComponent.prototype.discardChanges = function () {
        var id = this.recipe._id;
        this._router.navigate(['/recipes', id]);
    };
    RecipeEditComponent.prototype.save = function () {
        var _this = this;
        if (typeof this.recipe.tags.split != "undefined") {
            this.recipe.tags = this.recipe.tags.split(',');
            for (var i = 0; i < this.recipe.tags.length; i++) {
                this.recipe.tags[i] = this.recipe.tags[i].trim();
            }
        }
        if (this.mode == 'edit') {
            this._recipeService.updateRecipe(this.recipe)
                .then(function () {
                _this.goBack(true);
            });
        }
        else {
            this._recipeService.createRecipe(this.recipe)
                .then(function (resp) {
                if (resp._id) {
                    _this._router.navigate(['/recipes', resp._id]);
                }
            });
        }
    };
    RecipeEditComponent.prototype.addIngredient = function () {
        this.recipe.ingredients.push({});
    };
    RecipeEditComponent.prototype.deleteIngredient = function (ingredient) {
        this.deleteFrom(this.recipe.ingredients, ingredient);
    };
    RecipeEditComponent.prototype.addMethod = function () {
        if (typeof this.recipe.methods == "undefined")
            this.recipe.methods = [];
        this.recipe.methods.push({});
    };
    RecipeEditComponent.prototype.deleteMethod = function (method) {
        this.deleteFrom(this.recipe.methods, method);
    };
    RecipeEditComponent.prototype.addTag = function () {
        this.recipe.tags.push('');
    };
    RecipeEditComponent.prototype.deleteTag = function (tag) {
        this.deleteFrom(this.recipe.tags, tag);
    };
    RecipeEditComponent.prototype.fileChange = function (event) {
        var file = event.target.files[0];
        var filename = file.name.split('.');
        if (this.recipe._id) {
            this.recipe.photo = this.recipe._id + '.' + filename[filename.length - 1] + '?' + new Date().getTime();
        }
        this.recipe.file = file;
    };
    RecipeEditComponent.prototype.deleteFrom = function (variable, toFind) {
        var index = variable.indexOf(toFind);
        if (index > -1) {
            variable.splice(index, 1);
        }
    };
    RecipeEditComponent.prototype.deepCopy = function (obj) {
        var newObj;
        if (Object.prototype.toString.call(obj) === '[object Array]') {
            newObj = [];
            for (var _i = 0, obj_1 = obj; _i < obj_1.length; _i++) {
                var item = obj_1[_i];
                if (typeof item == "object") {
                    newObj.push(this.deepCopy(item));
                }
                else {
                    newObj.push(item);
                }
            }
        }
        else {
            newObj = Object.assign({}, obj);
            for (var key in newObj) {
                if (typeof newObj[key] == "object") {
                    newObj[key] = this.deepCopy(newObj[key]);
                }
            }
        }
        return newObj;
    };
    RecipeEditComponent.prototype.compare = function (obj1, obj2) {
        var diff = false;
        for (var key in obj1) {
            if (typeof obj2[key] == "undefined") {
                diff = true;
            }
            else if (typeof obj1[key] != typeof obj2[key]) {
                diff = true;
            }
            else if (typeof obj1[key] == "object") {
                if (this.compare(obj1[key], obj2[key])) {
                    diff = true;
                }
            }
            else if (obj1[key] != obj2[key]) {
                diff = true;
            }
        }
        return diff;
    };
    __decorate([
        core_1.ViewChild('discardModal'), 
        __metadata('design:type', modal_component_1.ModalComponent)
    ], RecipeEditComponent.prototype, "discardModal", void 0);
    __decorate([
        core_1.ViewChild('errorSnack'), 
        __metadata('design:type', notification_component_1.SnackBarComponent)
    ], RecipeEditComponent.prototype, "errorSnack", void 0);
    RecipeEditComponent = __decorate([
        core_1.Component({
            selector: 'recipe-edit',
            templateUrl: 'tpl/recipe-edit.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, recipes_service_1.RecipesService])
    ], RecipeEditComponent);
    return RecipeEditComponent;
}());
exports.RecipeEditComponent = RecipeEditComponent;
