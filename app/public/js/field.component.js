"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var InlineInputComponent = (function () {
    function InlineInputComponent(_el) {
        this._el = _el;
        this.type = 'text';
        this.required = false;
        this.placeholder = '';
        this.onFocus = new core_1.EventEmitter(false);
        this.onBlur = new core_1.EventEmitter(false);
        this.onModelChange = new core_1.EventEmitter(false);
        this.originalHeight = 0;
    }
    InlineInputComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.type == 'textarea') {
            setTimeout(function () {
                _this.originalHeight = _this.getHeight();
                _this.setHeight(_this.getHeight());
            }, 50);
        }
    };
    InlineInputComponent.prototype.focus = function () {
        this.onFocus.emit(undefined);
    };
    InlineInputComponent.prototype.blur = function () {
        this.onBlur.emit(undefined);
    };
    InlineInputComponent.prototype.modelChange = function () {
        this.onModelChange.emit(this.model);
        if (this.type == 'textarea') {
            this.setHeight(this.getHeight());
        }
    };
    InlineInputComponent.prototype.elementExists = function () {
        if (typeof this._el == "undefined" || !this._el)
            return false;
        else if (typeof this._el.nativeElement == "undefined")
            return false;
        else if (typeof this._el.nativeElement.firstElementChild == "undefined")
            return false;
        return true;
    };
    InlineInputComponent.prototype.getHeight = function (auto) {
        if (auto === void 0) { auto = true; }
        if (!this.elementExists())
            return;
        if (auto)
            this.setHeight('auto');
        return this._el.nativeElement.firstElementChild.scrollHeight + 2;
    };
    InlineInputComponent.prototype.setHeight = function (height) {
        if (!this.elementExists() || !height)
            return;
        if (this.model) {
            this._el.nativeElement.firstElementChild.style.height = height != 'auto' ? height + 'px' : this.originalHeight + 'px';
        }
        else {
            this._el.nativeElement.firstElementChild.style.height = false;
        }
    };
    __decorate([
        core_1.Input('type'), 
        __metadata('design:type', String)
    ], InlineInputComponent.prototype, "type", void 0);
    __decorate([
        core_1.Input('model'), 
        __metadata('design:type', Object)
    ], InlineInputComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('label'), 
        __metadata('design:type', Object)
    ], InlineInputComponent.prototype, "label", void 0);
    __decorate([
        core_1.Input('field'), 
        __metadata('design:type', Object)
    ], InlineInputComponent.prototype, "field", void 0);
    __decorate([
        core_1.Input('required'), 
        __metadata('design:type', Boolean)
    ], InlineInputComponent.prototype, "required", void 0);
    __decorate([
        core_1.Input('placeholder'), 
        __metadata('design:type', String)
    ], InlineInputComponent.prototype, "placeholder", void 0);
    __decorate([
        core_1.Output('focus'), 
        __metadata('design:type', core_1.EventEmitter)
    ], InlineInputComponent.prototype, "onFocus", void 0);
    __decorate([
        core_1.Output('blur'), 
        __metadata('design:type', core_1.EventEmitter)
    ], InlineInputComponent.prototype, "onBlur", void 0);
    __decorate([
        core_1.Output('modelChange'), 
        __metadata('design:type', core_1.EventEmitter)
    ], InlineInputComponent.prototype, "onModelChange", void 0);
    InlineInputComponent = __decorate([
        core_1.Component({
            selector: 'inline-input',
            template: "\n\t\t<textarea *ngIf=\"type=='textarea'\" [(ngModel)]=\"model\" (ngModelChange)=\"modelChange($event)\" [placeholder]=\"placeholder\" (focus)=\"focus()\" (blur)=\"blur()\" [attr.id]=\"field\" [required]=\"required\"></textarea>\n\t\t<input *ngIf=\"type!='textarea'\" [type]=\"type\" [(ngModel)]=\"model\" (ngModelChange)=\"modelChange($event)\" [placeholder]=\"placeholder\" (focus)=\"focus()\" (blur)=\"blur()\" [attr.id]=\"field\" [required]=\"required\" autocomplete=\"off\">"
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], InlineInputComponent);
    return InlineInputComponent;
}());
exports.InlineInputComponent = InlineInputComponent;
var InputComponent = (function () {
    function InputComponent() {
        this.type = 'text';
        this.required = false;
        this.placeholder = '';
        this.onModelChange = new core_1.EventEmitter(false);
    }
    InputComponent.prototype.ngOnInit = function () {
        this.filled = this.model != null && this.model.length > 0;
    };
    InputComponent.prototype.focus = function () {
        this.active = true;
    };
    InputComponent.prototype.blur = function () {
        this.active = false;
        this.filled = this.model != null && this.model.length > 0;
    };
    InputComponent.prototype.modelChange = function ($event) {
        this.onModelChange.emit(this.model);
    };
    __decorate([
        core_1.Input('type'), 
        __metadata('design:type', String)
    ], InputComponent.prototype, "type", void 0);
    __decorate([
        core_1.Input('model'), 
        __metadata('design:type', Object)
    ], InputComponent.prototype, "model", void 0);
    __decorate([
        core_1.Input('label'), 
        __metadata('design:type', Object)
    ], InputComponent.prototype, "label", void 0);
    __decorate([
        core_1.Input('field'), 
        __metadata('design:type', Object)
    ], InputComponent.prototype, "field", void 0);
    __decorate([
        core_1.Input('required'), 
        __metadata('design:type', Boolean)
    ], InputComponent.prototype, "required", void 0);
    __decorate([
        core_1.Input('placeholder'), 
        __metadata('design:type', String)
    ], InputComponent.prototype, "placeholder", void 0);
    __decorate([
        core_1.Output('modelChange'), 
        __metadata('design:type', core_1.EventEmitter)
    ], InputComponent.prototype, "onModelChange", void 0);
    InputComponent = __decorate([
        core_1.Component({
            selector: 'fancy-input',
            template: "\n\t<div class=\"row\" [class.active]=\"active\" [class.filled]=\"filled\">\n\t\t<label *ngIf=\"label\" [attr.for]=\"field\">{{label}}</label>\n\t\t<inline-input [type]=\"type\" [(model)]=\"model\" [placeholder]=\"placeholder\" (modelChange)=\"modelChange($event)\" (focus)=\"focus()\" (blur)=\"blur()\" [field]=\"field\" [required]=\"required\" ></inline-input>\n\t</div>"
        }), 
        __metadata('design:paramtypes', [])
    ], InputComponent);
    return InputComponent;
}());
exports.InputComponent = InputComponent;
exports.FIELD_DIRECTIVES = [
    InlineInputComponent,
    InputComponent
];
