"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('./user.service');
var router_1 = require('@angular/router');
var MenuComponent = (function () {
    function MenuComponent(_userService, _router) {
        this._userService = _userService;
        this._router = _router;
        this.showMenu = false;
        this.showSideMenu = false;
    }
    MenuComponent.prototype.toggleSideMenu = function () {
        this.showSideMenu = !this.showSideMenu;
        if (this.showSideMenu) {
            this.opened();
        }
        else {
            this.closed();
        }
    };
    MenuComponent.prototype.opened = function () {
        var _this = this;
        var focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';
        var modal = document.querySelector('.side-menu');
        var focusableElements = modal.querySelectorAll(focusableElementsString);
        focusableElements = Array.prototype.slice.call(focusableElements);
        this.firstTabStop = focusableElements[0];
        this.lastTabStop = focusableElements[focusableElements.length - 1];
        this.previousActive = document.activeElement;
        setTimeout(function () {
            _this.firstTabStop.focus();
        }, 50);
    };
    MenuComponent.prototype.closed = function () {
        this.previousActive.focus();
    };
    MenuComponent.prototype.logout = function () {
        this._userService.logout();
        this._router.navigate(['/login'], { queryParams: {} });
    };
    MenuComponent.prototype.goToCategory = function (category) {
        this._reset();
        this._router.navigate(['/recipes/categories', category]);
    };
    MenuComponent.prototype.goToAll = function () {
        this._reset();
        this._router.navigate(['/recipes']);
    };
    MenuComponent.prototype.trapKey = function (e) {
        // Check for TAB key press
        if (e.keyCode === 9) {
            // SHIFT + TAB
            if (e.shiftKey) {
                if (document.activeElement === this.firstTabStop) {
                    e.preventDefault();
                    this.lastTabStop.focus();
                }
            }
            else {
                if (document.activeElement === this.lastTabStop) {
                    e.preventDefault();
                    this.firstTabStop.focus();
                }
            }
        }
        // ESCAPE
        if (e.keyCode === 27) {
            this.toggleSideMenu();
        }
    };
    MenuComponent.prototype._reset = function () {
        this.toggleSideMenu();
        sessionStorage.setItem('recipes.scrollY', "0");
    };
    __decorate([
        core_1.Input('side-menu'), 
        __metadata('design:type', Boolean)
    ], MenuComponent.prototype, "showMenu", void 0);
    MenuComponent = __decorate([
        core_1.Component({
            selector: 'menu',
            templateUrl: 'tpl/menu.component.html'
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], MenuComponent);
    return MenuComponent;
}());
exports.MenuComponent = MenuComponent;
exports.MENU_DIRECTIVES = [
    MenuComponent
];
