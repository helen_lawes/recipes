"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_service_1 = require('./user.service');
/**
 * Login
 *
 * Handles logging in of the user and storing the json web token to local storage
 */
var LoginComponent = (function () {
    function LoginComponent(_route, _router, _userService) {
        this._route = _route;
        this._router = _router;
        this._userService = _userService;
        this.login = true;
        this.errorMessage = '';
        this.email = '';
        this.emailConfirm = '';
        this.password = '';
        this.passwordConfirm = '';
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.url = this._route.url.subscribe(function (url) {
            _this.login = url[0].path != 'register';
        });
    };
    LoginComponent.prototype.toggleForm = function (form) {
        this._reset();
        this.loading = false;
        this.login = (form == 'login');
    };
    LoginComponent.prototype._reset = function () {
        this.errorMessage = '';
    };
    LoginComponent.prototype.submitForm = function () {
        this._reset();
        this.loading = true;
        if (this.login)
            this.userLogin();
        else
            this.userRegister();
    };
    LoginComponent.prototype.userLogin = function () {
        var _this = this;
        var error = this._validate();
        // check login credentials with the user service
        if (!error)
            this._userService.login(this.email, this.password)
                .then(function (response) { return _this.handleResponse(response); });
        else
            this.loading = false;
    };
    LoginComponent.prototype.userRegister = function () {
        var _this = this;
        // send registration details with the user service
        var user = {
            email: this.email,
            emailConfirm: this.emailConfirm,
            password: this.password,
            passwordConfirm: this.passwordConfirm
        };
        var error = this._validate();
        if (!error)
            this._userService.register(user)
                .then(function (response) { return _this.handleResponse(response); });
        else
            this.loading = false;
    };
    LoginComponent.prototype._validate = function () {
        var error;
        var additional = !this.login ? "and confirm" : "";
        if (this.email == "") {
            this.errorMessage += "Please fill in email " + additional + "\n";
            error = true;
        }
        if (this.password == "") {
            this.errorMessage += "Please fill in password " + additional + "\n";
            error = true;
        }
        if (this.login)
            return error;
        if (this.email != this.emailConfirm) {
            this.errorMessage += "Email and confirm don't match\n";
            error = true;
        }
        if (this.password != this.passwordConfirm) {
            this.errorMessage += "Password and confirm don't match\n";
            error = true;
        }
        return error;
    };
    LoginComponent.prototype.storeToken = function (token) {
        window.localStorage.setItem('token', token);
    };
    LoginComponent.prototype.handleResponse = function (response) {
        this.loading = false;
        if (typeof response.status != "undefined") {
            // print to screen any error message sent back from the API
            this.errorMessage = response._body;
        }
        else {
            if (this.login) {
                // store the jwt after successful login and redirect to recipe list
                this.storeToken(response);
                this._router.navigate(['/recipes']);
            }
            else {
                this.login = true;
            }
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'tpl/login.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, user_service_1.UserService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
