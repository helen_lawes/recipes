"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var recipes_service_1 = require('./recipes.service');
var modal_component_1 = require("./modal.component");
var notification_component_1 = require('./notification.component');
/**
 * Recipe detail
 *
 * Displays all the information relating to a particular recipe
 */
var RecipeDetailComponent = (function () {
    function RecipeDetailComponent(_route, _router, _recipeService) {
        this._route = _route;
        this._router = _router;
        this._recipeService = _recipeService;
        this.showFab = false;
    }
    RecipeDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        window.scrollTo(0, 0);
        // get the id from the route params and request recipe from recipe service
        this.sub = this._route.params.subscribe(function (params) {
            var id = params['id'];
            _this._recipeService.getRecipe(id)
                .subscribe(function (recipe) { return _this.recipe = recipe; }, function (e) {
                _this.errorSnack.show();
            });
        });
    };
    RecipeDetailComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    RecipeDetailComponent.prototype.goToRecipes = function () {
        var category = sessionStorage.getItem('recipes.category');
        if (typeof category != "undefined" && category) {
            this._router.navigate(['/recipes/categories', category]);
        }
        else {
            this._router.navigate(['/recipes']);
        }
    };
    RecipeDetailComponent.prototype.editRecipe = function () {
        // go to recipe edit route
        if (!this.recipe)
            return;
        this._router.navigate(['/recipes', this.recipe._id, 'edit']);
    };
    RecipeDetailComponent.prototype.deleteRecipe = function () {
        if (!this.recipe)
            return;
        // open confirmation dialog
        this.deleteModal.open();
    };
    RecipeDetailComponent.prototype.confirmRecipeDelete = function () {
        var _this = this;
        if (!this.recipe)
            return;
        // delete recipe and then redirect back to the recipe listing
        this._recipeService.deleteRecipe(this.recipe)
            .then(function () {
            _this.goToRecipes();
        });
    };
    RecipeDetailComponent.prototype.toggleFab = function () {
        this.showFab = !this.showFab;
    };
    __decorate([
        core_1.ViewChild('deleteModal'), 
        __metadata('design:type', modal_component_1.ModalComponent)
    ], RecipeDetailComponent.prototype, "deleteModal", void 0);
    __decorate([
        core_1.ViewChild('errorSnack'), 
        __metadata('design:type', notification_component_1.SnackBarComponent)
    ], RecipeDetailComponent.prototype, "errorSnack", void 0);
    RecipeDetailComponent = __decorate([
        core_1.Component({
            selector: 'recipe-detail',
            templateUrl: 'tpl/recipe-detail.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, recipes_service_1.RecipesService])
    ], RecipeDetailComponent);
    return RecipeDetailComponent;
}());
exports.RecipeDetailComponent = RecipeDetailComponent;
