"use strict";
var router_1 = require('@angular/router');
var recipes_component_1 = require('./recipes.component');
var login_component_1 = require('./login.component');
var auth_guard_1 = require('./auth.guard');
var user_service_1 = require('./user.service');
var recipe_detail_component_1 = require('./recipe-detail.component');
var recipe_edit_component_1 = require('./recipe-edit.component');
var recipes_service_1 = require('./recipes.service');
var socket_service_1 = require('./socket.service');
var database_1 = require('./database');
/**
 * Router configuration
 *
 * routes with canActivate: [AuthGuard] require a valid json web token to access
 */
exports.routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'register', component: login_component_1.LoginComponent },
    { path: 'recipes', component: recipes_component_1.RecipesComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'recipes/categories/:category', component: recipes_component_1.RecipesComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'recipes/new', component: recipe_edit_component_1.RecipeEditComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'recipes/:id', component: recipe_detail_component_1.RecipeDetailComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'recipes/:id/edit', component: recipe_edit_component_1.RecipeEditComponent, canActivate: [auth_guard_1.AuthGuard] }
];
// require route provider along with AuthGuard, UserService and RecipesService as these are
// used throughout the app and only one global instance is created
exports.appRoutingProviders = [
    auth_guard_1.AuthGuard,
    user_service_1.UserService,
    recipes_service_1.RecipesService,
    socket_service_1.SocketService,
    database_1.Database
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
exports.routeComponents = [
    recipes_component_1.RecipesComponent,
    login_component_1.LoginComponent,
    recipe_detail_component_1.RecipeDetailComponent,
    recipe_edit_component_1.RecipeEditComponent
];
