"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var DatabaseObjectStore = (function () {
    function DatabaseObjectStore(_store) {
        var _this = this;
        this._store = _store;
        var funcs = ['add', 'clear', 'delete', 'get', 'getAll', 'getAllKeys', 'put'];
        funcs.forEach(function (func) {
            _this[func] = function (arg1, arg2) {
                return new Promise(function (resolve, reject) {
                    var action = _this._store[func](arg1, arg2);
                    action.onsuccess = function (event) {
                        resolve(action.result);
                    };
                    action.onerror = function (evt) {
                        reject(evt);
                    };
                });
            };
        });
    }
    DatabaseObjectStore.prototype.loopCursor = function (loopFunc) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var action = _this._store.openCursor();
            action.onsuccess = function (event) {
                var cursor = event.target.result;
                if (!cursor) {
                    resolve();
                    return;
                }
                loopFunc(cursor);
                cursor.continue();
            };
            action.onerror = function (e) {
                reject(e);
            };
        });
    };
    return DatabaseObjectStore;
}());
exports.DatabaseObjectStore = DatabaseObjectStore;
var DatabaseTransaction = (function () {
    function DatabaseTransaction(_tx) {
        this._tx = _tx;
    }
    DatabaseTransaction.prototype.objectStore = function (storeName) {
        this.store = this._tx.objectStore(storeName);
        return new DatabaseObjectStore(this.store);
    };
    return DatabaseTransaction;
}());
exports.DatabaseTransaction = DatabaseTransaction;
var DatabaseConnection = (function () {
    function DatabaseConnection(_dbRes) {
        this._dbRes = _dbRes;
    }
    DatabaseConnection.prototype.transaction = function (storeName, mode) {
        if (mode === void 0) { mode = 'readonly'; }
        this.tx = this._dbRes.transaction(storeName, mode);
        return new DatabaseTransaction(this.tx);
    };
    return DatabaseConnection;
}());
exports.DatabaseConnection = DatabaseConnection;
var Database = (function () {
    function Database() {
    }
    Database.prototype.open = function (name, version, upgraded) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.request = window.indexedDB.open(name, version);
            _this.request.onupgradeneeded = function (event) {
                _this.db = event.target.result;
                if (typeof upgraded == "function") {
                    upgraded(_this.db);
                }
                resolve(new DatabaseConnection(_this.db));
            };
            _this.request.onsuccess = function (event) {
                _this.db = event.target.result;
                resolve(new DatabaseConnection(_this.db));
            };
            _this.request.onerror = function () {
                reject();
            };
        });
    };
    Database = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], Database);
    return Database;
}());
exports.Database = Database;
