"use strict";
var KeyboardEvents = (function () {
    function KeyboardEvents() {
        this.focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';
    }
    KeyboardEvents.prototype.triggerClick = function (e, func) {
        var keyCode = e.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            var args = [];
            for (var i = 2; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            func.apply(this, args);
        }
    };
    return KeyboardEvents;
}());
exports.KeyboardEvents = KeyboardEvents;
