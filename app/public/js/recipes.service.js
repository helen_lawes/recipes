"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var database_1 = require('./database');
var utilities_1 = require('./utilities');
var socket_service_1 = require('./socket.service');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/throw');
require('rxjs/add/operator/catch');
require('rxjs/add/operator/debounceTime');
require('rxjs/add/operator/distinctUntilChanged');
require('rxjs/add/operator/map');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/toPromise');
require('rxjs/add/operator/retry');
/**
 * Recipe service
 *
 * Interacts with the backend API
 */
var RecipesService = (function () {
    function RecipesService(_http, _db, _socket) {
        var _this = this;
        this._http = _http;
        this._db = _db;
        this._socket = _socket;
        // event emitter for service worker
        this.swEvents = new core_1.EventEmitter();
        // base url for recipe api
        this.baseUrl = '/api/recipes';
        this._liveData = [];
        this.data = [];
        this.status = 'init';
        this._dbName = 'recipes';
        this._dbStoreName = 'recipes';
        this._dbUp = true;
        this._dbEmpty = true;
        this._recipeEmitter = new core_1.EventEmitter();
        // register the service worker
        this._registerServiceWorker();
        // open the connection to the IndexedDB
        this._dbInit().then(function () {
            // run live recipe sync when able to connect to servers socket.io
            _this._socket.getConnection().subscribe(function (connection) {
                if (connection)
                    _this._getLiveRecipesSync();
            });
        });
        // HTTP headers for requests
        this._headers = {
            json: new http_1.Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }),
            form: new http_1.Headers({
                'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            })
        };
    }
    RecipesService.prototype.getRecipes = function () {
        var _this = this;
        if (this._dbUp) {
            // get recipes from local database
            this._dbGetRecipes()
                .then(function () {
                _this._recipeEmitter.emit(_this.data);
            });
        }
        else {
            // if local IndexedDB is offline then grab live data
            this._getLiveRecipesSync(false);
        }
        // Display a message to the user if local & live DBs are offline
        if (!this._dbUp && this.status == 'offline') {
            this._recipeEmitter.error('No database found');
        }
        return this._recipeEmitter;
    };
    RecipesService.prototype.getRecipe = function (id) {
        var _this = this;
        return new Observable_1.Observable(function (observer) {
            if (_this._dbUp) {
                _this._dbGetRecipe(id).then(function (recipe) { return observer.next(recipe); }).catch(function (e) { return observer.error(e); });
            }
            else if (_this.data.length) {
                var match = _this._filterById(_this.data, id);
                if (match) {
                    observer.next(match);
                    observer.complete();
                }
                else {
                    observer.error('None found');
                }
            }
            else {
                _this.getRecipes().subscribe(function () {
                    var match = _this._filterById(_this.data, id);
                    if (match) {
                        observer.next(match);
                        observer.complete();
                    }
                    else {
                        observer.error('None found');
                    }
                }, function (e) {
                    observer.error(e);
                });
            }
            return function () { };
        });
    };
    RecipesService.prototype._filterById = function (data, id) {
        var match = data.filter(function (h) { return h._id === id; });
        return match[0] || false;
    };
    RecipesService.prototype._excludeByIds = function (data, ids) {
        var matches = data.filter(function (h) { return !ids.includes(h._id); });
        return matches.length ? matches : false;
    };
    RecipesService.prototype.getBlank = function () {
        // create a blank recipe object
        return Promise.resolve({
            "_id": new utilities_1.ObjectId().toString(),
            "photo": "",
            "name": "",
            "brief_description": "",
            "tags": [],
            "serves": null,
            "time": null,
            "ingredients": [],
            "methods": [],
            "personal_notes": "",
            "recipe_refs": "",
            "category": "",
            "timestamp": new Date().getTime()
        });
    };
    RecipesService.prototype.toFormObject = function (recipe) {
        // convert object data into FormData to POST file information
        var file = recipe.file;
        delete recipe.file;
        var formData = new FormData();
        formData.append("recipe", JSON.stringify(recipe));
        formData.append("file", file);
        return formData;
    };
    RecipesService.prototype.updateRecipe = function (recipe) {
        var _this = this;
        return new Promise(function (resolve) {
            recipe.timestamp = new Date().getTime();
            // update the recipe data in the recipe service as the data is not edited live
            _this.data.forEach(function (v, k) {
                if (v._id == recipe._id) {
                    _this.data[k] = recipe;
                }
            });
            // store recipe changes locally in DB
            _this._dbStoreRecipe(recipe).then(resolve);
            // send recipe changes to live DB, but don't wait for response to speed up app response
            _this._httpUpdateRecipe(recipe);
        });
    };
    RecipesService.prototype._httpUpdateRecipe = function (recipe) {
        var options = new http_1.RequestOptions({ headers: this._headers.form });
        var data = this.toFormObject(recipe);
        return this._http.put(this.baseUrl + "/" + recipe._id, data, options)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    RecipesService.prototype.createRecipe = function (recipe) {
        var _this = this;
        return new Promise(function (resolve) {
            // store the new recipe in the local DB
            _this._dbStoreRecipe(recipe).then(resolve);
            // merge the new recipe in with the other recipes in the service
            _this.data.push(recipe);
            // send newly created recipe to live DB
            _this._httpCreateRecipe(recipe);
        });
    };
    RecipesService.prototype._httpCreateRecipe = function (recipe) {
        var options = new http_1.RequestOptions({ headers: this._headers.form });
        var data = this.toFormObject(recipe);
        return this._http.post("" + this.baseUrl, data, options)
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    RecipesService.prototype.deleteRecipe = function (recipe) {
        var _this = this;
        return new Promise(function (resolve) {
            var index = _this.data.indexOf(recipe);
            if (index > -1) {
                _this.data.splice(index, 1);
            }
            _this._dbDeleteRecipe(recipe._id).then(resolve);
            var options = new http_1.RequestOptions({ headers: _this._headers.json, body: '' });
            _this._http.delete(_this.baseUrl + "/" + recipe._id, options)
                .toPromise()
                .then(function (response) { return response.json(); })
                .catch(_this.handleError);
        });
    };
    RecipesService.prototype._httpDeleteRecipes = function (ids) {
        var options = new http_1.RequestOptions({ headers: this._headers.json, body: ids });
        this._http.delete("" + this.baseUrl, options)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RecipesService.prototype._registerServiceWorker = function () {
        var _this = this;
        if (!navigator.serviceWorker)
            return;
        navigator.serviceWorker.register('/sw.js').then(function (reg) {
            if (!navigator.serviceWorker.controller) {
                return;
            }
            if (reg.waiting) {
                _this._swUpdateReady(reg.waiting);
                return;
            }
            if (reg.installing) {
                _this._swTrackInstall(reg.installing);
                return;
            }
            if (reg.active) {
                _this._swActive(reg.active);
            }
            reg.addEventListener('updatefound', function () {
                _this._swTrackInstall(reg.installing);
            });
        });
        var refreshing;
        navigator.serviceWorker.addEventListener('controllerchange', function () {
            if (refreshing)
                return;
            window.location.reload();
            refreshing = true;
        });
    };
    RecipesService.prototype._swTrackInstall = function (worker) {
        var _this = this;
        worker.addEventListener('statechange', function () {
            if (worker.state == 'installed') {
                _this._swUpdateReady(worker);
            }
        });
    };
    RecipesService.prototype._swUpdateReady = function (worker) {
        // emit service worker ready event
        this.swEvents.emit('ready');
        this._swWorker = worker;
    };
    RecipesService.prototype._swActive = function (worker) {
        // emit service worker active event
        this.swEvents.emit('active');
        this._swWorker = worker;
    };
    RecipesService.prototype.swUpdateConfirmed = function () {
        // send message to service worker to update to latest version
        this._swWorker.postMessage({ action: 'skipWaiting' });
    };
    RecipesService.prototype.swClearPhotos = function (imagesUsed) {
        var _this = this;
        if (this._swWorker) {
            this._swWorker.postMessage({ action: 'clearPhotos', imagesUsed: imagesUsed });
        }
        else {
            this.swEvents.subscribe(function (message) {
                if (message == 'ready' || message == 'active')
                    _this._swWorker.postMessage({ action: 'clearPhotos', imagesUsed: imagesUsed });
            });
        }
    };
    RecipesService.prototype._dbInit = function () {
        var _this = this;
        return this._dbPromise = this._db.open(this._dbName, 2, function (upgradeDb) {
            upgradeDb.createObjectStore(_this._dbStoreName, { keyPath: '_id' });
        }).catch(function () {
            _this._dbUp = false;
        });
    };
    RecipesService.prototype._dbGetRecipes = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                store.getAll().then(function (recipes) {
                    if (recipes.length) {
                        _this._dbEmpty = false;
                        _this.data = recipes;
                        resolve();
                    }
                }).catch(function (e) { return reject(e); });
            });
        });
    };
    RecipesService.prototype._dbGetRecipe = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                store.get(id).then(function (recipe) {
                    resolve(recipe);
                }).catch(function (e) { return reject(e); });
            });
        });
    };
    RecipesService.prototype._dbStoreRecipes = function (recipes) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                recipes.forEach(function (recipe) {
                    store.put(recipe).catch(function (e) { return reject(e); });
                });
                resolve();
            });
        });
    };
    RecipesService.prototype._dbStoreRecipe = function (recipe) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                store.put(recipe).then(function () { return resolve(recipe); }).catch(reject);
            });
        });
    };
    RecipesService.prototype._getLiveRecipesSync = function (sync) {
        var _this = this;
        if (sync === void 0) { sync = true; }
        var options = new http_1.RequestOptions({ headers: this._headers.json, body: '' });
        this._http.get("" + this.baseUrl, options)
            .retry(5)
            .subscribe(function (response) {
            _this._liveData = response.json();
            _this.status = 'success';
            if (sync || !_this._dbUp) {
                _this._dbSyncRecipes().then(function () {
                    _this._recipeEmitter.emit(_this.data);
                });
            }
            else {
                _this.data = _this._liveData;
            }
        }, function (e) {
            _this.status = 'offline';
        });
    };
    RecipesService.prototype._dbSyncRecipes = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var syncTime = new Date().getTime();
            var lastSync = localStorage.getItem('dbSync') / 1 || syncTime;
            var ids = [];
            var imagesUsed = [];
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                store.loopCursor(function (cursor) {
                    var current = cursor.value;
                    // check if current db record is in live data
                    var match = _this._filterById(_this._liveData, current._id);
                    var using = true;
                    if (match) {
                        if (current.timestamp > match.timestamp) {
                            // if local db record more up-to-date then send update to server
                            _this._httpUpdateRecipe(current);
                        }
                        else {
                            // if live db record more up-to-date then store in local
                            _this._dbStoreRecipe(match);
                        }
                    }
                    else {
                        if (current.timestamp > lastSync) {
                            // if local db record newer than last sync then send to server
                            _this._httpCreateRecipe(current).catch(function () {
                                current.timestamp = syncTime + 5;
                                _this._dbStoreRecipe(current);
                            });
                        }
                        else {
                            // if local db record older than sync then delete from local
                            using = false;
                            cursor.delete();
                        }
                    }
                    if (using) {
                        ids.push(current._id);
                        imagesUsed.push(current.photo);
                    }
                }).then(function () {
                    var unsaved = _this._excludeByIds(_this._liveData, ids);
                    var toSave = [];
                    var toDelete = [];
                    if (unsaved) {
                        // only save down recipes added after last sync as they have been added
                        // older recipes would have been deleted
                        unsaved.forEach(function (recipe) {
                            if (recipe.timestamp > lastSync || !ids.length) {
                                toSave.push(recipe);
                            }
                            else {
                                toDelete.push(recipe._id);
                            }
                        });
                        if (toSave.length)
                            _this._dbStoreRecipes(toSave);
                        if (toDelete.length)
                            _this._httpDeleteRecipes(toDelete);
                    }
                    localStorage.setItem('dbSync', '' + new Date().getTime());
                    _this.swClearPhotos(imagesUsed);
                    _this._dbGetRecipes().then(function () { return resolve(_this.data); });
                }).catch(reject);
            }).catch(function () {
                _this.data = _this._liveData;
                resolve();
            });
        });
    };
    RecipesService.prototype._dbDeleteRecipe = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._dbPromise.then(function (db) {
                var store = db.transaction(_this._dbStoreName, 'readwrite').objectStore(_this._dbStoreName);
                store.delete(id).then(resolve).catch(reject);
            });
        });
    };
    RecipesService.prototype.handleError = function (error) {
        return Promise.resolve(error.message || error);
    };
    RecipesService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, database_1.Database, socket_service_1.SocketService])
    ], RecipesService);
    return RecipesService;
}());
exports.RecipesService = RecipesService;
