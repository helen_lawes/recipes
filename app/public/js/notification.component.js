"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SnackBarComponent = (function () {
    function SnackBarComponent() {
        this.shown = false;
        this.button = false;
        this.onAction = new core_1.EventEmitter(false);
    }
    SnackBarComponent.prototype.show = function () {
        this.shown = true;
    };
    SnackBarComponent.prototype.hide = function () {
        this.shown = false;
    };
    SnackBarComponent.prototype.action = function () {
        this.hide();
        this.onAction.emit(undefined);
    };
    __decorate([
        core_1.Input('button'), 
        __metadata('design:type', Object)
    ], SnackBarComponent.prototype, "button", void 0);
    __decorate([
        core_1.Output('action'), 
        __metadata('design:type', core_1.EventEmitter)
    ], SnackBarComponent.prototype, "onAction", void 0);
    SnackBarComponent = __decorate([
        core_1.Component({
            selector: 'snackbar',
            template: "<div class=\"snackbar\" [class.shown]=\"shown\">\n\t\t<div class=\"snackbar-content\"><ng-content></ng-content></div>\n\t\t<div class=\"snackbar-button\" *ngIf=\"button\" (click)=\"action()\">{{button}}</div>\n\t</div>"
        }), 
        __metadata('design:paramtypes', [])
    ], SnackBarComponent);
    return SnackBarComponent;
}());
exports.SnackBarComponent = SnackBarComponent;
exports.NOTIFICATION_DIRECTIVES = [
    SnackBarComponent
];
