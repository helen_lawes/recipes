"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SocketService = (function () {
    function SocketService() {
        var _this = this;
        this._connection = new core_1.EventEmitter();
        if (io) {
            var path = window.location.protocol + '//' + window.location.hostname + ':8080';
            this._socket = io.connect(path);
            this._socket.on('connect', function () {
                _this._connection.emit(true);
            });
            this._socket.on('disconnect', function () {
                _this._connection.emit(false);
            });
            this._socket.on('error', function (e) {
                // TODO: intercept errors
            });
            this._socket.on('message', function (message) {
                // TODO: intercept messages
            });
        }
    }
    SocketService.prototype.getConnection = function () {
        return this._connection;
    };
    SocketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
