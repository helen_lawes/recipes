"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var JoinPipe = (function () {
    function JoinPipe() {
    }
    JoinPipe.prototype.transform = function (v, args) {
        if (typeof v.join != "function")
            return v;
        return v.join(args ? args[0] : ', ');
    };
    JoinPipe = __decorate([
        core_1.Pipe({ name: 'join' }), 
        __metadata('design:paramtypes', [])
    ], JoinPipe);
    return JoinPipe;
}());
exports.JoinPipe = JoinPipe;
var HideEmptyPipe = (function () {
    function HideEmptyPipe() {
    }
    HideEmptyPipe.prototype.transform = function (v, args) {
        if (typeof v == "undefined" || !v)
            return '';
        return v;
    };
    HideEmptyPipe = __decorate([
        core_1.Pipe({ name: 'hideEmpty' }), 
        __metadata('design:paramtypes', [])
    ], HideEmptyPipe);
    return HideEmptyPipe;
}());
exports.HideEmptyPipe = HideEmptyPipe;
var TruncatePipe = (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (v, length) {
        if (typeof v == "undefined" || !v)
            return '';
        if (typeof v == "string") {
            var parts = v.split(' ');
            var string = "";
            var temp = "";
            for (var i = 0; i < parts.length; i++) {
                temp += " " + parts[i];
                if (temp.length < length) {
                    string = temp;
                }
                else {
                    return string.substr(1) + '...';
                }
            }
            return string.substr(1);
        }
        return v;
    };
    TruncatePipe = __decorate([
        core_1.Pipe({ name: 'truncate' }), 
        __metadata('design:paramtypes', [])
    ], TruncatePipe);
    return TruncatePipe;
}());
exports.TruncatePipe = TruncatePipe;
var HumanTimePipe = (function () {
    function HumanTimePipe() {
    }
    HumanTimePipe.prototype.transform = function (input, args) {
        var mins = parseInt(input);
        if (!isNaN(mins)) {
            var hours = Math.floor(mins / 60);
            if (hours > 0) {
                mins = (mins - (hours * 60));
            }
            var v = (hours ? hours + ' hr' + (hours > 1 ? 's' : '') + ' ' : '') + (mins > 0 ? mins + ' mins' : '');
            return v;
        }
        else {
            return '';
        }
    };
    HumanTimePipe = __decorate([
        core_1.Pipe({ name: 'humanTime' }), 
        __metadata('design:paramtypes', [])
    ], HumanTimePipe);
    return HumanTimePipe;
}());
exports.HumanTimePipe = HumanTimePipe;
var LinkyPipe = (function () {
    function LinkyPipe() {
        this.LINKY_URL_REGEXP = /((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"\u201d\u2019]/i;
        this.MAILTO_REGEXP = /^mailto:/i;
    }
    LinkyPipe.prototype.transform = function (text, target, attributes) {
        if (text == null || text === '' || !isString(text))
            return text;
        var attributesFn = isFunction(attributes) ? attributes :
            isObject(attributes) ? function getAttributesObject() { return attributes; } :
                function getEmptyAttributesObject() { return {}; };
        var match;
        var raw = text;
        var html = [];
        var url;
        var i;
        while ((match = raw.match(this.LINKY_URL_REGEXP))) {
            // We can not end in these as they are sometimes found at the end of the sentence
            url = match[0];
            // if we did not match ftp/http/www/mailto then assume mailto
            if (!match[2] && !match[4]) {
                url = (match[3] ? 'http://' : 'mailto:') + url;
            }
            i = match.index;
            addText(raw.substr(0, i));
            addLink(url, match[0].replace(this.MAILTO_REGEXP, ''));
            raw = raw.substring(i + match[0].length);
        }
        addText(raw);
        return html.join('');
        function isString(text) {
            return typeof text == "string";
        }
        function isFunction(attributes) {
            return typeof attributes == "function";
        }
        function isObject(object) {
            return typeof object == "object";
        }
        function isDefined(defined) {
            return typeof defined != "undefined";
        }
        function addText(text) {
            if (!text) {
                return;
            }
            html.push(text);
        }
        function addLink(url, text) {
            var key, linkAttributes = attributesFn(url);
            html.push('<a ');
            for (key in linkAttributes) {
                html.push(key + '="' + linkAttributes[key] + '" ');
            }
            if (isDefined(target) && !('target' in linkAttributes)) {
                html.push('target="', target, '" ');
            }
            html.push('href="', url.replace(/"/g, '&quot;'), '">');
            addText(text);
            html.push('</a>');
        }
    };
    LinkyPipe = __decorate([
        core_1.Pipe({ name: 'linky' }), 
        __metadata('design:paramtypes', [])
    ], LinkyPipe);
    return LinkyPipe;
}());
exports.LinkyPipe = LinkyPipe;
exports.FORMATTING_PIPES = [
    JoinPipe,
    HideEmptyPipe,
    TruncatePipe,
    HumanTimePipe,
    LinkyPipe
];
