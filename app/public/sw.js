//1530266790014
var cacheFiles = [
	"/",
	"/css/styles.css",
	"/js/bundle.js",
	"/core-js/client/shim.min.js",
	"/zone.js/dist/zone.js",
	"/reflect-metadata/Reflect.js",
	"/socket.io/socket.io.js",
	"/resources/cooking.jpg",
	"/resources/cook_hat.png",
	"/resources/cook_hat.svg",
	"/resources/cook_hat_med.png",
	"/resources/cook_hat_med_inverted.png",
	"/resources/cook_hat_small.png",
	"/resources/cook_hat_small_inverted.png",
	"/resources/cook_hat_white.svg",
	"/resources/default.jpg",
	"/resources/favicon.png",
	"/tpl/login.html",
	"/tpl/menu.component.html",
	"/tpl/recipe-detail.html",
	"/tpl/recipe-edit.html",
	"/tpl/recipes.html"
];
var staticCacheName = 'recipes-static-v1';
var contentImgsCache = 'recipes-content-imgs';
var allCaches = [
	staticCacheName,
	contentImgsCache
];


self.addEventListener('install',function(event){
	event.waitUntil(
		caches.open(staticCacheName).then(function (cache) {
			return cache.addAll(cacheFiles || [
				'/',
				'/css/styles.css'
			]);
		})
	);
});

self.addEventListener('activate',function(event){
	event.waitUntil(
		caches.keys().then(function(cacheNames){
			return Promise.all(
				cacheNames.filter(function(cacheName){
					return cacheName.startsWith('recipes-') &&
							!allCaches.includes(cacheName);
				}).map(function(cacheName){
					return caches.delete(cacheName)
				})
			)
		})
	);
});

self.addEventListener('fetch', function(event) {
	var requestUrl = new URL(event.request.url);

	if (requestUrl.origin === location.origin) {
		if(requestUrl.pathname === '/login' || requestUrl.pathname === '/register' || requestUrl.pathname.startsWith('/recipes')){
			event.respondWith(caches.match('/'));
			return;
		}
		if(requestUrl.pathname.startsWith('/api/') || requestUrl.pathname.startsWith('/browser-sync/')){
			return;
		}
		if(requestUrl.pathname.startsWith('/images/')){
			event.respondWith(servePhoto(event.request));
			return;
		}
	}
	event.respondWith(
		caches.match(event.request).then(function(response) {
			return response || fetch(event.request);
		})
	);
});

self.addEventListener('message',function(event){
	if(event.data.action == 'skipWaiting'){
		self.skipWaiting();
	} else if(event.data.action == 'clearPhotos'){
		clearOldCache(event.data.imagesUsed);
	}
});

function servePhoto(request){
	var storageUrl = request.url;
	return caches.open(contentImgsCache).then(function(cache) {
		return cache.match(storageUrl).then(function(response) {
			if (response) return response;
			return fetch(request).then(function(networkResponse) {
				cache.put(storageUrl, networkResponse.clone());
				return networkResponse;
			});
		});
	});
}
function clearOldCache(images){
	return caches.open(contentImgsCache).then(function(cache){
		return cache.keys().then(function(files){
			files.forEach(function(request){
				var path = request.url.split('images/')[1];
				if(!images.includes(path)){
					cache.delete(request);
				}
			});
		})
	});
}